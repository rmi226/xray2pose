#! /usr/bin/env python3

# For plotting with PIL
from PIL import Image

# For plotting with matplotlib
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

from matplotlib import animation, rc
from IPython.display import HTML

from scipy.optimize import minimize
import numpy as np

import gvxrPython3 as gvxr
import scipy.io

def getRelativeErrorMap(aReferenceImage, aTestImage, EPSILON = 0.00001):
    return np.absolute(
        ((aReferenceImage.astype("float") + EPSILON) - (aTestImage.astype("float") + EPSILON)) / 
            (aReferenceImage.astype("float") + EPSILON));






def getAbsoluteErrorMap(aReferenceImage, aTestImage):
    return np.absolute((aReferenceImage.astype("float") - aTestImage.astype("float")));

def computeSAE(aReferenceImage, aTestImage):
    return np.sum(getAbsoluteErrorMap(aReferenceImage, aTestImage));



def Numpy2PIL(anImage):
    # Normalise the image between 0 and 255 (this is for PIL)
    np_normalised_image = (255 * (anImage-anImage.min())/anImage.max()).astype(np.int8);
    
    # Convert the Numpy array into a PIL image
    return Image.fromarray(np_normalised_image.astype(np.ubyte));


def PIL2Numpy(anImage):
    return np.array(anImage).astype("float");

def normalizeMinMaxImage(anImage):
    min_value = anImage.min();
    max_value = anImage.max();
    
    return (anImage - min_value) / (max_value - min_value);

def normalizeMeanStdevImage(anImage):
    mean_value = anImage.mean();
    stddev_value = anImage.std();
    
    return (anImage - mean_value) / (stddev_value);




#def computeShapeAngles(X, Y):
    




# Open the image in grayscale with PIL
pil_fixed_image = Image.open("xrays/1.jpg").convert('L');

hand_shape = scipy.io.loadmat("xrays/1.mat")


progress_im_size = [128, 128]


# Show the fixed image with PIL
#pil_fixed_image.show();

# Convert the image into a Numpy array
np_fixed_image = PIL2Numpy(pil_fixed_image)

np_fixed_image = np.fliplr(np_fixed_image)

import scipy.misc
np_fixed_image = scipy.misc.imresize(np_fixed_image, [progress_im_size[0], progress_im_size[1]])



X = progress_im_size[0] - hand_shape['wholeHand'][:, 0] / np.max(hand_shape['wholeHand'][:, 0]) * progress_im_size[0]
Y = hand_shape['wholeHand'][:, 1] / np.max(hand_shape['wholeHand'][:, 1]) * progress_im_size[1]

plt.figure()
plt.imshow(np_fixed_image,zorder=1)
plt.scatter(X, Y, zorder=2, c=[1, 0, 0])
plt.show()





# Normalise it between 0 and 1
np_normalized_fixed_image = normalizeMinMaxImage(np_fixed_image);


# Compute the negative
np_normalized_fixed_image = 1.0 - np_normalized_fixed_image ;

# Show the fixed image with matplotlib
plt.figure()
plt.imshow(np_normalized_fixed_image, cmap = plt.get_cmap('hot'))
plt.title('Fixed image');

# Normalise it 
np_normalized_fixed_image = normalizeMeanStdevImage(np_normalized_fixed_image) * 1.5;







# Create an OpenGL context
print("Create an OpenGL context")
gvxr.createWindow();
gvxr.setWindowSize(512, 512);


# Set up the beam
print("Set up the beam")
gvxr.setSourcePosition(40.0,  0.0, 0.0, "cm");
gvxr.usePointSource();
#gvxr.useParallelBeam();
gvxr.setMonoChromatic(0.08, "MeV", 1000);

# Set up the detector
print("Set up the detector");
gvxr.setDetectorPosition(-2.0, 0.0, 0.0, "cm");

gvxr.setDetectorUpVector(0, 1, 0);

gvxr.setDetectorNumberOfPixels(np_normalized_fixed_image.shape[1], np_normalized_fixed_image.shape[0]);

gvxr.setDetectorPixelSize(1.5, 1.5, "mm");

# Load the data
print("Load the data");
gvxr.loadSceneGraph("hand.dae", "mm");

print ("Number of meshes in scenegraph:\t" + str(len(gvxr.getMeshLabelSet())))
for (idx, mesh) in enumerate(gvxr.getMeshLabelSet()):
    #print("Set the mesh's Hounsfield unit");
    #print(mesh)
    gvxr.setHU(mesh, 1000)
    




gvxr.moveToCentre();
#
gvxr.scaleScene(400, 400, 400, "mm");





#exit()

#gvxr.rotateNode("node-Mid_Prox", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Mid_Midd", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Mid_Dist", -45, 0.0, 1.0, 0.0);

#gvxr.rotateNode("node-Ind_Prox", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Ind_Midd", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Ind_Dist", -45, 0.0, 1.0, 0.0);

#gvxr.rotateNode("node-Lit_Prox", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Lit_Midd", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Lit_Dist", -45, 0.0, 1.0, 0.0);

#gvxr.rotateNode("node-Thi_Prox", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Thi_Midd", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Thi_Dist", -45, 0.0, 1.0, 0.0);

#gvxr.rotateNode("node-Thu_Prox", -45, 0.0, 1.0, 0.0);
#gvxr.rotateNode("node-Thu_Dist", -45, 0.0, 1.0, 0.0);






nodes = ['node-Thu_Meta', 'node-Thu_Prox', 'node-Thu_Dist']


def save_node_transforms():
    m = []
    for node in nodes:
        m.append(gvxr.getNodeTransformationMatrix(node))
    return m

def save_all_node_transforms():
    m = []
    for node in gvxr.getMeshLabelSet():
        m.append(gvxr.getNodeTransformationMatrix(node))
    return m
        


def restore_node_transforms(m):
    i = 0
    for node in nodes:
        gvxr.setNodeTransformationMatrix(node, m[i])
        i = i + 1
        

def transform(p, c):
    
    if(c[0]):
        gvxr.translateScene(p[0], p[1], p[2], 'mm')    
        gvxr.rotateScene(p[3], 1, 0, 0)
        gvxr.rotateScene(p[4], 0, 1, 0)
        gvxr.rotateScene(p[5], 0, 0, 1)
    
    m = save_node_transforms()
    
    
    if(c[1]):
        for i in range(len(nodes)):
            gvxr.rotateNode(nodes[i], p[i+6], 1.0, 0.0, 0.0);
    
    return m


def show(p, c):
    m = transform(p, c)    
    np_image = gvxr.computeXRayImage();
    #restore_node_transforms(m)
    np_image = np.array(np_image) # gvxr.getNumpyImage();
    np_image = normalizeMeanStdevImage(np_image);
    absolute_error = getAbsoluteErrorMap(np_normalized_fixed_image, np_image);
    plt.figure()
    im = plt.imshow(np_image, cmap = plt.get_cmap('jet'))
    plt.figure()
    im = plt.imshow(absolute_error, cmap = plt.get_cmap('jet'))


def objective(p, c):
    gvxr.setSceneTransformationMatrix(orig_scene)
    transform(p, c)    
    np_image = gvxr.computeXRayImage();
    np_image = np.array(np_image) # gvxr.getNumpyImage();
    np_image = normalizeMeanStdevImage(np_image);
    absolute_error = getAbsoluteErrorMap(np_normalized_fixed_image, np_image);
    cost = np.sum(absolute_error)/10000
    if(np.isnan(cost)):
        cost = 10000000000
    print(cost)
    return cost





orig_transforms = save_all_node_transforms();

orig_scene = gvxr.getSceneTransformationMatrix()        


#
# this function below should generate a snapshot v-xray of each mesh 
# in the model, but only works for some 
#

def measure_bone_orientations(origTransforms):
        for (idx, mesh) in enumerate(gvxr.getMeshLabelSet()):
            for (i, m) in enumerate(gvxr.getMeshLabelSet()):
                gvxr.setHU(m, 100)
                if(i==idx):
                    gvxr.setNodeTransformationMatrix(m, origTransforms[i])
                else:
                    gvxr.translateNode(m, 100, 100, 100, 'm')
            np_image = gvxr.computeXRayImage();
            np_image = np.array(np_image) # gvxr.getNumpyImage();
            #np_image = normalizeMeanStdevImage(np_image);
            plt.figure()
            plt.imshow(np_image)



measure_bone_orientations(orig_transforms)




#initial = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
#c = [True, False]
#res = minimize(objective, initial, args=c, method='COBYLA', options={'ftol': 1e-2, 'disp': True})


#restore_node_transforms(orig_transforms)
#show(initial, c)


#show([0, 0, 0, 0, 0, 0, 0, 0, 0])



