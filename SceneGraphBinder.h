#ifndef __SceneGraphBinder_h
#define __SceneGraphBinder_h

#include <string>

// For polygon meshes
#ifndef __PolygonMesh_h
#include "gVirtualXRay/PolygonMesh.h"
#endif

// For MATRIX4
#ifndef __Types_h
#include "gVirtualXRay/Types.h"
#endif


class SceneGraphBinder
{
public:
	SceneGraphBinder();
	SceneGraphBinder(const SceneGraphBinder& aSceneGraph);
	virtual ~SceneGraphBinder();

	SceneGraphBinder& operator=(const SceneGraphBinder& aSceneGraph);


	const std::vector<gVirtualXRay::PolygonMesh>& getPolygonMeshSet() const;


	std::vector<gVirtualXRay::PolygonMesh>& getPolygonMeshSet();


	virtual void updateMatrices() = 0;


	void display(GLint aShaderID);


	void scale(const std::string& aNodeLabel,
			const gVirtualXRay::VEC3& aScalingFactorSet);

	virtual void scale(const char* aNodeLabel,
			const gVirtualXRay::VEC3& aScalingFactorSet) = 0;

	void rotate(const std::string& aNodeLabel,
			float aRotationAngleInDegrees,
			const gVirtualXRay::VEC3& aRotationAxis);


	virtual void rotate(const char* aNodeLabel,
			float aRotationAngleInDegrees,
			const gVirtualXRay::VEC3& aRotationAxis) = 0;


	gVirtualXRay::MATRIX4 getTransformationMatrix(const std::string& aNodeLabel) const;


	virtual gVirtualXRay::MATRIX4 getTransformationMatrix(const char* aNodeLabel) const = 0;


protected:
	std::vector<gVirtualXRay::PolygonMesh> m_p_polygon_mesh_set;
};


#endif // __SceneGraphBinder_h
