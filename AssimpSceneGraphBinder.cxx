#ifdef HAS_GLEW
#include <GL/glew.h>
#endif

#ifndef GVXR_CONFIG_H
#include "gVirtualXRay/gVirtualXRayConfig.h"
#endif

#include <limits>
#include <fstream>
#include <cstdio>

// ASSIMP to load Collada files
#include <assimp/scene.h>
#include <assimp/matrix4x4.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>


#ifndef __AssimpSceneGraphBinder_h
#include "AssimpSceneGraphBinder.h"
#endif

// For types such as VEC3, MATRIX4
#ifndef __Types_h
#include "gVirtualXRay/Types.h"
#endif

// For exceptions
#ifndef __Exception_h
#include "gVirtualXRay/Exception.h"
#endif

#ifndef __FileDoesNotExistException_h
#include "gVirtualXRay/FileDoesNotExistException.h"
#endif





//-----------------------------------------------
AssimpSceneGraphBinder::AssimpSceneGraphBinder():
//-----------------------------------------------
		SceneGraphBinder(),
		m_p_scene(0)
//-----------------------------------------------
{}


//----------------------------------------------------------------------------------------
AssimpSceneGraphBinder::AssimpSceneGraphBinder(const AssimpSceneGraphBinder& aSceneGraph):
//----------------------------------------------------------------------------------------
		SceneGraphBinder(aSceneGraph)
//----------------------------------------------------------------------------------------
{
	setSceneGraph(aSceneGraph.m_p_scene);
}


//-----------------------------------------------
AssimpSceneGraphBinder::~AssimpSceneGraphBinder()
//-----------------------------------------------
{}


//--------------------------------------------------------------------------------------------------
AssimpSceneGraphBinder& AssimpSceneGraphBinder::operator=(const AssimpSceneGraphBinder& aSceneGraph)
//--------------------------------------------------------------------------------------------------
{
	SceneGraphBinder::operator=(aSceneGraph);

	setSceneGraph(aSceneGraph.m_p_scene);

	return (*this);
}


//---------------------------------------------------------------------
void AssimpSceneGraphBinder::setSceneGraph(const aiScene* apSceneGraph)
//---------------------------------------------------------------------
{
	// Empty the scenegraph
	m_p_polygon_mesh_set.clear();

	if (m_p_scene != apSceneGraph)
	{
		m_p_scene = const_cast<aiScene*>(apSceneGraph);
	}

	
	int i = 0;
	recursivePolygonMeshBuild(m_p_scene->mRootNode, aiMatrix4x4(), i);
	
}


//----------------------------------------------------------------
void AssimpSceneGraphBinder::loadSceneGraph(const char* aFileName)
//----------------------------------------------------------------
{
	// Check if file exists
	std::ifstream file_stream(aFileName);

	// The file exist
	if(!file_stream.fail())
	{
		file_stream.close();
	}
	// The file doest not exist
	else
	{
		// Throw an error
		throw gVirtualXRay::FileDoesNotExistException(__FILE__, __FUNCTION__, __LINE__, aFileName);
	}

	// Load the file
	const aiScene* p_scene = m_importer.ReadFile( aFileName, aiProcessPreset_TargetRealtime_Quality);

	// The import failed
	if (!p_scene)
	{
		throw gVirtualXRay::Exception(__FILE__, __FUNCTION__, __LINE__, m_importer.GetErrorString());
	}

	// Load and set the scenegraph
	setSceneGraph(p_scene);
}


//-----------------------------------------------------------------------
void AssimpSceneGraphBinder::loadSceneGraph(const std::string& aFileName)
//-----------------------------------------------------------------------
{
	loadSceneGraph(aFileName.data());
}


//-------------------------------------------
void AssimpSceneGraphBinder::updateMatrices()
//-------------------------------------------
{
	int i = 0;
	recursiveUpdateMatrices(m_p_scene->mRootNode, aiMatrix4x4(), i);
}






//-----------------------------------------------------------------------------
void AssimpSceneGraphBinder::scale(const char* aNodeLabel,
                                   const gVirtualXRay::VEC3& aScalingFactorSet)
//-----------------------------------------------------------------------------
{
	// Find the node
	aiNode* p_node = findNode(aNodeLabel);

	// The node has been found
	if (p_node)
	{
		// Create the transformation matrix
		aiMatrix4x4 scaling_matrix;

		scaling_matrix = aiMatrix4x4::Scaling(
				aiVector3t<float>(aScalingFactorSet[0], aScalingFactorSet[1], aScalingFactorSet[2]),
				scaling_matrix);

		// Update the node's transformation matrix
		p_node->mTransformation = scaling_matrix * p_node->mTransformation;
	}
	// The node has not been found
	else
	{
		throw gVirtualXRay::Exception(__FILE__, __FUNCTION__, __LINE__, "Unknown node");
	}
}


//--------------------------------------------------------------------------
void AssimpSceneGraphBinder::rotate(const char* aNodeLabel,
								    float aRotationAngleInDegrees,
							        const gVirtualXRay::VEC3& aRotationAxis)
//--------------------------------------------------------------------------
{
	// Find the node
	aiNode* p_node = findNode(aNodeLabel);

	// The node has been found
	if (p_node)
	{
		// Create the rotation matrix
		aiMatrix4x4 rotation_matrix;

		rotation_matrix = aiMatrix4x4::Rotation(
				M_PI * aRotationAngleInDegrees /180.0,
				aiVector3t<float>(aRotationAxis[0], aRotationAxis[1], aRotationAxis[2]),
				rotation_matrix);

		// Update the node's transformation matrix
		p_node->mTransformation = p_node->mTransformation * rotation_matrix;
	}
	// The node has not been found
	else
	{
		throw gVirtualXRay::Exception(__FILE__, __FUNCTION__, __LINE__, "Unknown node");
	}
}



//--------------------------------------------------------------------------
void AssimpSceneGraphBinder::translate(const char* aNodeLabel,
							        const gVirtualXRay::VEC3& aTranslationAmount)
//--------------------------------------------------------------------------
{
	// Find the node
	aiNode* p_node = findNode(aNodeLabel);

	// The node has been found
	if (p_node)
	{
		// Create the rotation matrix
		aiMatrix4x4 translation_matrix;

		translation_matrix = aiMatrix4x4::Translation(aiVector3t<float>(aTranslationAmount[0], aTranslationAmount[1], aTranslationAmount[2]),  translation_matrix);

		// Update the node's transformation matrix
		p_node->mTransformation = p_node->mTransformation * translation_matrix;
	}
	// The node has not been found
	else
	{
		throw gVirtualXRay::Exception(__FILE__, __FUNCTION__, __LINE__, "Unknown node");
	}
}


//-------------------------------------------------------------------------------------------------
gVirtualXRay::MATRIX4 AssimpSceneGraphBinder::getTransformationMatrix(const char* aNodeLabel) const
//-------------------------------------------------------------------------------------------------
{
	// Delete next line and add code here to update the transformation matrices in m_p_polygon_mesh_set
	throw gVirtualXRay::Exception(__FILE__, __FUNCTION__, __LINE__, "Not implemented");

	return (gVirtualXRay::MATRIX4());
}


//--------------------------------------------------------------------------
const aiNode* AssimpSceneGraphBinder::findNode(const char* aNodeLabel) const
//--------------------------------------------------------------------------
{
	return (recursiveFindNode(m_p_scene->mRootNode, aNodeLabel));
}


//---------------------------------------------------------------------------------
const aiNode* AssimpSceneGraphBinder::findNode(const std::string& aNodeLabel) const
//---------------------------------------------------------------------------------
{
	return (findNode(aNodeLabel.data()));
}


//--------------------------------------------------------------
aiNode* AssimpSceneGraphBinder::findNode(const char* aNodeLabel)
//--------------------------------------------------------------
{
	return (recursiveFindNode(m_p_scene->mRootNode, aNodeLabel));
}


//---------------------------------------------------------------------
aiNode* AssimpSceneGraphBinder::findNode(const std::string& aNodeLabel)
//---------------------------------------------------------------------
{
	return (findNode(aNodeLabel.data()));
}


void AssimpSceneGraphBinder::recursivePolygonMeshBuild(const aiNode* nd, aiMatrix4x4 tStack, int &anIndex)
{
	std::cout << nd->mName.C_Str() << "\t" << nd->mNumChildren << "\t" << nd->mNumMeshes << std::endl;

	aiMatrix4x4 m =  nd->mTransformation ;


	tStack = tStack * m;

	
	aiMatrix4x4 temp_matrix = tStack;
	temp_matrix.Transpose();

	gVirtualXRay::MATRIX4 modelling_matrix(
			temp_matrix.a1, temp_matrix.a2, temp_matrix.a3, temp_matrix.a4,
			temp_matrix.b1, temp_matrix.b2, temp_matrix.b3, temp_matrix.b4,
			temp_matrix.c1, temp_matrix.c2, temp_matrix.c3, temp_matrix.c4,
			temp_matrix.d1, temp_matrix.d2, temp_matrix.d3, temp_matrix.d4);

	

	for (unsigned int n=0; n < nd->mNumMeshes; ++n)
	{
		std::vector<float> p_vertex_set;
		std::vector<unsigned int> p_index_array;

		const aiMesh* mesh = m_p_scene->mMeshes[nd->mMeshes[n]];

		for (int t = 0; t < mesh->mNumFaces; ++t)
		{
			const aiFace* face = &mesh->mFaces[t];
			GLenum face_mode;


			for(int i = 0; i < face->mNumIndices; i++)
			{
				p_index_array.push_back(face->mIndices[i]);
			}
		}

		for (int t = 0; t < mesh->mNumVertices; ++t)
		{
			aiVector3D vertex(mesh->mVertices[t]);

			p_vertex_set.push_back( (vertex.x));
			p_vertex_set.push_back( (vertex.y));
			p_vertex_set.push_back( (vertex.z));
		}

		if (p_vertex_set.size() && p_index_array.size())
		{
			m_p_polygon_mesh_set.push_back(gVirtualXRay::PolygonMesh());

			m_p_polygon_mesh_set[m_p_polygon_mesh_set.size()-1].setInternalData(GL_TRIANGLES,
				&p_vertex_set,
				&p_index_array,
				true,
				GL_STATIC_DRAW);

			m_p_polygon_mesh_set[m_p_polygon_mesh_set.size()-1].computeNormalVectors();
			m_p_polygon_mesh_set[m_p_polygon_mesh_set.size()-1].setTransformationMatrix(modelling_matrix);
		}
	}


	// all children
	for (unsigned int n=0; n < nd->mNumChildren; ++n)
	{
		anIndex++;
		recursivePolygonMeshBuild(nd->mChildren[n], tStack, anIndex);
	}
}


void AssimpSceneGraphBinder::recursiveUpdateMatrices(const aiNode* nd, aiMatrix4x4 tStack, int& anIndex)
{
	//std::cout << nd->mName.C_Str() << "\t" << nd->mNumChildren << "\t" << nd->mNumMeshes << std::endl;

	aiMatrix4x4 m =  nd->mTransformation ;
	tStack = tStack * m;

	aiMatrix4x4 temp_matrix = tStack;

	temp_matrix.Transpose();

	gVirtualXRay::MATRIX4 modelling_matrix(
			temp_matrix.a1, temp_matrix.a2, temp_matrix.a3, temp_matrix.a4,
			temp_matrix.b1, temp_matrix.b2, temp_matrix.b3, temp_matrix.b4,
			temp_matrix.c1, temp_matrix.c2, temp_matrix.c3, temp_matrix.c4,
			temp_matrix.d1, temp_matrix.d2, temp_matrix.d3, temp_matrix.d4);

	std::vector<float> p_vertex_set;

	for (unsigned int n=0; n < nd->mNumMeshes; ++n)
	{
		if (nd->mMeshes)
		{
			m_p_polygon_mesh_set[anIndex++].setTransformationMatrix(modelling_matrix);
		}
	}

	// all children
	for (unsigned int n=0; n < nd->mNumChildren; ++n)
	{
		recursiveUpdateMatrices(nd->mChildren[n], tStack, anIndex);
	}
}











//-----------------------------------------------------------------------------
const aiNode* AssimpSceneGraphBinder::recursiveFindNode(const aiNode* nd,
														const char* aNodeLabel) const
//-----------------------------------------------------------------------------
{
	const aiNode* p_node = 0;


	//std::cout << nd->mName.C_Str() << "\t" <<  std::endl;

	// The node has been found
	if (!strcmp(nd->mName.C_Str(), aNodeLabel))
	{
		p_node = nd;
	}
	// The node has not been found
	else
	{
		// all children
		for (unsigned int n=0; n < nd->mNumChildren; ++n)
		{
			if (!p_node)
			{
				p_node = recursiveFindNode(nd->mChildren[n], aNodeLabel);
			}
		}
	}

	return (p_node);
}


//-----------------------------------------------------------------------
aiNode* AssimpSceneGraphBinder::recursiveFindNode(aiNode* nd,
												  const char* aNodeLabel)
//-----------------------------------------------------------------------
{
	aiNode* p_node = 0;


	//std::cout << nd->mName.C_Str() << "\t" <<  std::endl;

	// The node has been found
	if (!strcmp(nd->mName.C_Str(), aNodeLabel))
	{
		p_node = nd;
	}
	// The node has not been found
	else
	{
		// all children
		for (unsigned int n=0; n < nd->mNumChildren; ++n)
		{
			if (!p_node)
			{
				p_node = recursiveFindNode(nd->mChildren[n], aNodeLabel);
			}
		}
	}

	return (p_node);
}


