//******************************************************************************
//  Include
//******************************************************************************
#ifdef HAS_GLEW
#include <GL/glew.h>
#endif

#ifndef GVXR_CONFIG_H
#include "gVirtualXRay/gVirtualXRayConfig.h"
#endif

#include <algorithm>
#include <cstdlib>
#include <sstream>
#include <streambuf>
#include <ctime>

#include "SOIL/SOIL.h"

#define GLFW_INCLUDE_GLCOREARB 1

#include <GLFW/glfw3.h>


// For types such as VEC3, MATRIX4
#ifndef __Types_h
#include "gVirtualXRay/Types.h"
#endif

// For polygon meshes
#ifndef __PolygonMesh_h
#include "gVirtualXRay/PolygonMesh.h"
#endif

// To set the energy of the photons in the incident beam
#ifndef __XRayBeam_h
#include "gVirtualXRay/XRayBeam.h"
#endif

// To set the properties of the X-ray detector
#ifndef __XRayDetector_h
#include "gVirtualXRay/XRayDetector.h"
#endif

// To perform the computations of the X-ray simulation
#ifndef __XRayRenderer_h
#include "gVirtualXRay/XRayRenderer.h"
#endif

// To initialise GLEW, to define matrix stacks, to define state stacks, etc.
#ifndef __OpenGLUtilities_h
#include "gVirtualXRay/OpenGLUtilities.h"
#endif


#ifndef __AssimpSceneGraphBinder_h
#include "AssimpSceneGraphBinder.h"
#endif


#include "display_gl2.frag.h"
#include "display_gl2.vert.h"

#include "display_gl3.frag.h"
#include "display_gl3.vert.h"



#include <ceres/ceres.h>




//******************************************************************************
//  Name space
//******************************************************************************
using namespace gVirtualXRay;


using ceres::NumericDiffCostFunction;
using ceres::CENTRAL;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;



//******************************************************************************
//  Function declaration
//******************************************************************************
void quit();


void initGLFW();


void initGL();


void errorCallback(int error, const char* description);


void framebufferSizeCallback(GLFWwindow* apWindow, int aWidth, int aHeight);


void keyCallback(GLFWwindow* apWindow,
    int aKey,
    int aScanCode,
    int anAction,
    int aModifierKey);


void mouseButtonCallback(GLFWwindow* apWindow,
    int aButton,
    int aButtonState,
    int aModifierKey);


void cursorPosCallback(GLFWwindow* apWindow, double x, double y);


void scrollCallback(GLFWwindow* apWindow, double xoffset, double yoffset);


Vec3<GLfloat> getArcballVector(int x, int y);


float radian2degree(float anAngle);


void computeRotation(MATRIX4& aRotationMatrix);


void loadSceneGraph();


void loadDetector();


void loadSource();


void loadXRaySimulator();


void updateXRayImage();


void draw();


void render();


//******************************************************************************
//  Constant variables
//******************************************************************************
const GLfloat g_rotation_speed(2.0);

const float g_incident_energy(0.08 * MeV);

const float g_scaling_factor(2.5);


const VEC2 g_resolution(1.0 * g_scaling_factor * mm, 1.0 * g_scaling_factor * mm);
const Vec2ui g_number_of_pixels(250 / g_scaling_factor, 250 / g_scaling_factor);

const double g_initial_fovy(45);        // Field of view along the y-axis
const double g_initial_near(5.0 * cm);  // Near clipping plane
const double g_initial_far(500.0 * cm); // Far clipping plane

const VEC3 g_initial_source_position(   0.0,  50.0 * cm, 0.0);
const VEC3 g_initial_detector_position( 0.0, -25.0 * cm, 0.0);
const VEC3 g_initial_detector_up_vector(0.0, 0.0, 1.0);
const VEC3 g_background_colour(0.5, 0.5, 0.5);


//******************************************************************************
//  Global variables
//******************************************************************************
GLsizei g_main_window_width( 600 );
GLsizei g_main_window_height( 600 );
GLFWwindow* g_p_main_window_id(0);

GLfloat g_zoom(100.0 * cm);

int g_button(-1);
int g_button_state(-1);
bool g_use_arc_ball(false);
GLint g_last_x_position(0);
GLint g_last_y_position(0);
GLint g_current_x_position(0);
GLint g_current_y_position(0);


XRayBeam g_xray_beam;
XRayDetector g_xray_detector;
XRayRenderer g_xray_renderer;
Shader g_display_shader;

bool g_is_xray_image_up_to_date = false;

bool g_display_beam(true);


Matrix4x4<GLfloat> g_scene_rotation_matrix;
Matrix4x4<GLfloat> g_rotation_matrix;

AssimpSceneGraphBinder g_scene_graph;


Image<float> g_fixed_image;
Image<float> g_moving_image;
Image<float> g_error_image;

GLuint programID;
int width, height;

GLuint vertexbuffer;
GLuint uvbuffer;
unsigned int texture;

static const GLfloat g_vertex_buffer_data[] = {
   1.0f, 1.0f,	0.0f,
   1.0f, 0.0f,		0.0f,
   0.0f, 0.0f,		0.0f,
   1.0f, 1.0f,	0.0f,
   0.0f, 1.0f,		0.0f,
   0.0f, 0.0f,		0.0f,   
};
static const GLfloat g_uv_buffer_data[] = { 
		0.0f, 0.0f, 
		0.0f, 1.0f, 
		1.0f, 1.0f,
		0.0f, 0.0f, 
		1.0f, 0.0f, 
		1.0f, 1.0f
};

//******************************************************************************
//  Implementation
//******************************************************************************


// optimization function thoughts 
// - try rigid body first to see if that works 

				// rotation along VEC 1, 0, 0
				// rotation along VEC 0, 1, 0
				// rotation along VEC 0, 0, 1
				// translation along VEC 1, 0, 0
				// translation along VEC 0, 1, 0
				// translation along VEC 0, 0, 1


double poseCost = 0.0f; 


//double x[17] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};



//---------
void quit()
//---------
{
    if (g_p_main_window_id)
        {
        glfwDestroyWindow(g_p_main_window_id);
        g_p_main_window_id = 0;
        glfwTerminate();
        }
}


//-------------
void initGLFW()
//-------------
{
	// Set an error callback
	glfwSetErrorCallback(errorCallback);

	// Register the exit callback
	atexit(quit);

	// Initialize GLFW
	if (!glfwInit())
		{
		throw Exception(__FILE__, __FUNCTION__, __LINE__,
			"ERROR: cannot initialise GLFW.");
		}

	// Set the version of OpenGL that is required
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

	// Enable anti-aliasing
	glfwWindowHint(GLFW_SAMPLES, 4);

	// Create a windowed mode window and its OpenGL context
	g_p_main_window_id = glfwCreateWindow(g_main_window_width,
		g_main_window_height,
		"gVirtualXRay -- OpenSceneGraph Demo -- GLFW3",
		0,
		0);

	// Window cannot be created
	if (!g_p_main_window_id)
		{
		glfwTerminate();
		throw Exception(__FILE__, __FUNCTION__, __LINE__,
			"ERROR: cannot create a GLFW windowed mode window and "
			"its OpenGL context.");
		}

	// Make the window's context current
	glfwMakeContextCurrent(g_p_main_window_id);

	// Initialize GLFW callbacks
	glfwSetKeyCallback(g_p_main_window_id, keyCallback);
	glfwSetMouseButtonCallback(g_p_main_window_id, mouseButtonCallback);
	glfwSetCursorPosCallback(g_p_main_window_id, cursorPosCallback);
	glfwSetScrollCallback(g_p_main_window_id, scrollCallback);
	glfwSetFramebufferSizeCallback(g_p_main_window_id,
		framebufferSizeCallback);
}


//-----------
void initGL()
//-----------
{
    std::cout << "OpenGL renderer:   " <<
        glGetString(GL_RENDERER)   << std::endl;

    std::cout << "OpenGL version:    " <<
        glGetString(GL_VERSION)    << std::endl;

    std::cout << "OpenGL vender:     " <<
        glGetString(GL_VENDOR)     << std::endl;

    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    // Enable the Z-buffer
    glEnable(GL_DEPTH_TEST);
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    // Set the background colour
    glClearColor(g_background_colour.getX(),
        g_background_colour.getY(),
        g_background_colour.getZ(),
        1.0);
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    glEnable(GL_MULTISAMPLE);
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    // Initialise the shaders
    char* p_vertex_shader(0);
    char* p_fragment_shader(0);

    int z_lib_return_code_vertex(0);
    int z_lib_return_code_fragment(0);

    std::string vertex_shader;
    std::string fragment_shader;

    // Display shader
    if (useOpenGL3_2OrAbove())
        {
        z_lib_return_code_vertex   = inflate(g_display_gl3_vert,
            sizeof(g_display_gl3_vert),
            &p_vertex_shader);

        z_lib_return_code_fragment = inflate(g_display_gl3_frag,
            sizeof(g_display_gl3_frag),
            &p_fragment_shader);

        g_display_shader.setLabels("display_gl3.vert", "display_gl3.frag");
        }
    else
        {
        z_lib_return_code_vertex   = inflate(g_display_gl2_vert,
            sizeof(g_display_gl2_vert),
            &p_vertex_shader);

        z_lib_return_code_fragment = inflate(g_display_gl2_frag,
            sizeof(g_display_gl2_frag),
            &p_fragment_shader);

        g_display_shader.setLabels("display_gl2.vert", "display_gl2.frag");
        }

    vertex_shader   = p_vertex_shader;
    fragment_shader = p_fragment_shader;

    delete [] p_vertex_shader;
    delete [] p_fragment_shader;

    p_vertex_shader = 0;
    p_fragment_shader = 0;

    if (z_lib_return_code_vertex <= 0 || z_lib_return_code_fragment <= 0 ||
            !vertex_shader.size() || !fragment_shader.size())
        {
        throw Exception(__FILE__, __FUNCTION__, __LINE__,
            "Cannot decode the shader using ZLib.");
        }
    g_display_shader.loadSource(vertex_shader, fragment_shader);
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);
}


//----------------------------------------------------
void errorCallback(int error, const char* description)
//----------------------------------------------------
{
	// Display the error in the terminal
    std::cerr << "GLFW error: " << description << std::endl;
}


//-----------------------------------------------------------------------
void framebufferSizeCallback(GLFWwindow* apWindow, int width, int height)
//-----------------------------------------------------------------------
{
    glViewport(0, 0, width, height);

    if (height == 0)
        {
        // Prevent divide by 0
        height = 1;
        }

    g_main_window_width = width;
    g_main_window_height = height;
}


//-------------------------------------------------------------------------------
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
//-------------------------------------------------------------------------------
{
    updateXRayImage();

    
    if (action == GLFW_PRESS || action == GLFW_REPEAT)
        {
        switch (key)
            {
        	case GLFW_KEY_ESCAPE:
        		glfwSetWindowShouldClose(g_p_main_window_id, GL_TRUE);
        		break;
			




			case GLFW_KEY_Q:
				g_scene_graph.rotate("MaxScene", -1, VEC3(0.0, 1.0, 0.0));
				break;

			case GLFW_KEY_A:
				g_scene_graph.rotate("MaxScene", 1, VEC3(0.0, 1.0, 0.0));
				break;

			case GLFW_KEY_W:
				g_scene_graph.rotate("MaxScene",-1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_S:
				g_scene_graph.rotate("MaxScene", 1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_E:
				g_scene_graph.rotate("MaxScene",-1, VEC3(0.0, 0.0, 1.0));
				break;

			case GLFW_KEY_D:
				g_scene_graph.rotate("MaxScene", 1, VEC3(0.0, 0.0, 1.0));
				break;


			case GLFW_KEY_1:
				g_scene_graph.rotate("node-Thu_Meta",-1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_Z:
				g_scene_graph.rotate("node-Thu_Meta", 1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_2:
				g_scene_graph.rotate("node-Ind_Meta",-1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_X:
				g_scene_graph.rotate("node-Ind_Meta", 1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_3:
				g_scene_graph.rotate("node-Mid_Meta",-1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_C:
				g_scene_graph.rotate("node-Mid_Meta", 1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_4:
				g_scene_graph.rotate("node-Thi_Meta",-1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_V:
				g_scene_graph.rotate("node-Thi_Meta", 1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_5:
				g_scene_graph.rotate("node-Lit_Meta",-1, VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_B:
				g_scene_graph.rotate("node-Lit_Meta", 1, VEC3(1.0, 0.0, 0.0));
				break;



			case GLFW_KEY_T:
				g_scene_graph.translate("MaxScene", VEC3(-1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_G:
				g_scene_graph.translate("MaxScene", VEC3(1.0, 0.0, 0.0));
				break;

			case GLFW_KEY_Y:
				g_scene_graph.translate("MaxScene", VEC3(0.0, 1.0, 0.0));
				break;

			case GLFW_KEY_H:
				g_scene_graph.translate("MaxScene", VEC3(0.0, -1.0, 0.0));
				break;

			case GLFW_KEY_U:
				g_scene_graph.translate("MaxScene", VEC3(0.0, 0.0, 1.0));
				break;

			case GLFW_KEY_J:
				g_scene_graph.translate("MaxScene", VEC3(0.0, 0.0, -1.0));
				break;



            }

        }

    g_is_xray_image_up_to_date = false;
}


//--------------------------------------------
void mouseButtonCallback(GLFWwindow* apWindow,
                         int aButton,
                         int aButtonState,
                         int aModifierKey)
//--------------------------------------------
{
    g_button = aButton;
    g_button_state = aButtonState;

    // Use the arc ball
    if (g_button_state == GLFW_PRESS)
        {
        g_use_arc_ball = true;
        }
    // Stop using the arc ball
    else
        {
        g_use_arc_ball = false;
        }

    double xpos(0);
    double ypos(0);
    glfwGetCursorPos (apWindow, &xpos, &ypos);

    g_last_x_position = xpos;
    g_last_y_position = ypos;

    g_current_x_position = xpos;
    g_current_y_position = ypos;
}


//--------------------------------------------------------------
void cursorPosCallback(GLFWwindow* apWindow, double x, double y)
//--------------------------------------------------------------
{
    g_current_x_position = x;
    g_current_y_position = y;

    // Update the rotation matrix if needed
    computeRotation(g_scene_rotation_matrix);
}


//-----------------------------------------------------------------------
void scrollCallback(GLFWwindow* apWindow, double xoffset, double yoffset)
//-----------------------------------------------------------------------
{
    // Scrolling along the Y-axis
    if (fabs(yoffset) > EPSILON)
        {
        g_use_arc_ball = false;

        g_zoom += yoffset * cm;

        framebufferSizeCallback(apWindow,
            g_main_window_width,
            g_main_window_height);
        }
}


//------------------------------------------
Vec3<GLfloat> getArcballVector(int x, int y)
//------------------------------------------
{
    Vec3<GLfloat> P(2.0 * float(x) / float(g_main_window_width) - 1.0,
        2.0 * float(y) / float(g_main_window_height) - 1.0,
        0);

    P.setY(-P.getY());

    float OP_squared = P.getX() * P.getX() + P.getY() * P.getY();
    if (OP_squared <= 1.0)
        {
        P.setZ(sqrt(1.0 - OP_squared));  // Pythagore
        }
    else
        {
        P.normalise();  // nearest point
        }

    return (P);
}


//--------------------------------
float radian2degree(float anAngle)
//--------------------------------
{
    return (180.0 * anAngle / gVirtualXRay::PI);
}


//--------------------------------------------
void computeRotation(MATRIX4& aRotationMatrix)
//--------------------------------------------
{
    if (g_use_arc_ball)
        {
        if (g_current_x_position != g_last_x_position ||
                g_current_y_position != g_last_y_position)
            {
            Vec3<GLfloat> va(getArcballVector(g_last_x_position,
                g_last_y_position));

            Vec3<GLfloat> vb(getArcballVector(g_current_x_position,
                g_current_y_position));

            float angle(g_rotation_speed *
                radian2degree(acos(std::min(1.0, va.dotProduct(vb)))));

            Vec3<GLfloat> axis_in_camera_coord(va ^ vb);

            Matrix4x4<GLfloat> camera2object(aRotationMatrix.getInverse());
            Vec3<GLfloat> axis_in_object_coord =
                camera2object * axis_in_camera_coord;

            Matrix4x4<GLfloat> rotation_matrix;
            rotation_matrix.rotate(angle, axis_in_object_coord);
            aRotationMatrix = aRotationMatrix * rotation_matrix;

            g_last_x_position = g_current_x_position;
            g_last_y_position = g_current_y_position;
            }
        }
}


//-------------------
void loadSceneGraph()
//-------------------
{
    g_xray_renderer.removeInnerSurfaces();
    g_xray_renderer.removeOuterSurface();

	g_scene_graph.loadSceneGraph("./hand.dae");


	// this should be part of some pose initialization routine  
	g_scene_graph.rotate("MaxScene", 90, VEC3(0, 1, 0));
	g_scene_graph.rotate("MaxScene", 90, VEC3(1, 0, 0));
	g_scene_graph.rotate("MaxScene", -10, VEC3(0, 0, 1));

	g_scene_graph.scale("MaxScene", VEC3(350, 350, 350));


	short HU = 100;

	for (std::vector<PolygonMesh>::iterator ite = g_scene_graph.getPolygonMeshSet().begin();
			ite != g_scene_graph.getPolygonMeshSet().end();
			++ite)
	{
		ite->setHounsfieldValue(HU += HU);
		g_xray_renderer.addInnerSurface(&(*ite));
	}
}


//-----------------
void loadDetector()
//-----------------
{
    g_xray_detector.setResolutionInUnitsOfLengthPerPixel(g_resolution);
    g_xray_detector.setNumberOfPixels(g_number_of_pixels);
    g_xray_detector.setDetectorPosition(g_initial_detector_position);
    g_xray_detector.setUpVector(g_initial_detector_up_vector);

    // The X-ray image is not up-to-date
    g_is_xray_image_up_to_date = false;
}


//---------------
void loadSource()
//---------------
{
    // Set the energy
    g_xray_beam.initialise(g_incident_energy);

    // Set the source position
    g_xray_detector.setXrayPointSource(g_initial_source_position);

    // Use a parallel source
    //g_xray_detector.setParallelBeam();
    g_xray_detector.setConeBeam();

    // The X-ray image is not up-to-date
    g_is_xray_image_up_to_date = false;
}


//----------------------
void loadXRaySimulator()
//----------------------
{
    // Initialise the X-ray renderer
    g_xray_renderer.initialise(XRayRenderer::OPENGL,
        GL_RGB32F,
        &g_xray_detector,
        &g_xray_beam);

    // The X-ray image is not up-to-date
    g_is_xray_image_up_to_date = false;
}


//--------------------
void updateXRayImage()
//--------------------
{
    // The X-ray image is not up-to-date
    if (!g_is_xray_image_up_to_date)
        {
        float start_time(glfwGetTime());

        // Compute the X-Ray image
		g_xray_renderer.computeImage(g_rotation_matrix);

        // Normalise the X-ray image
		g_xray_renderer.getEnergyFluenceMinMax();

        // The X-ray image is up-to-date
        g_is_xray_image_up_to_date = true;
        }
}




GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path){

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);


	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open()){
		std::stringstream sstr;
		sstr << VertexShaderStream.rdbuf();
		VertexShaderCode = sstr.str();
		VertexShaderStream.close();
	}else{
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::stringstream sstr;
		sstr << FragmentShaderStream.rdbuf();
		FragmentShaderCode = sstr.str();
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}

	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}
	
	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);
	
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}


int iii = 0;



void setup_error_texture() {

	// Use our shader
	glUseProgram(programID);


	// This will identify our vertex buffer
	// Generate 1 buffer, put the resulting identifier in vertexbuffer
	glGenBuffers(1, &vertexbuffer);
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_uv_buffer_data), g_uv_buffer_data, GL_STATIC_DRAW);


	glGenTextures(0, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glActiveTexture(GL_TEXTURE1);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	
	//glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, g_fixed_image.getWidth(), g_fixed_image.getHeight(), 0, GL_LUMINANCE, GL_FLOAT, g_fixed_image.getRawData());



}






void updateTexture() {

	glUseProgram(programID);


	// Normalise the negative image between 0 and 1
	//g_moving_image = 1.0f - g_xray_renderer.getEnergyFluence().normalised();

	// More robust normalization
	g_moving_image = 1.0f - g_xray_renderer.getEnergyFluence();
	g_moving_image -= g_moving_image.getAverage();

	try {
		g_moving_image /= g_moving_image.getStandardDeviation();
	}
	catch(int e) {

	};
	// Compute Absolute error map (for display)
	g_error_image = (g_moving_image - g_fixed_image).abs().normalised();

	// Note to speed up computations, it will be fast to compute the positive of the fixed image (negative of the negative)

	//g_moving_image.save("moving_image.mha");
	//g_error_image.save("error_image.mha");

	//std::cout << "cost: " << cost << std::endl;
	//poseCost = 1.0 - g_fixed_image.computeNCC(g_moving_image);
	poseCost = g_fixed_image.computeSAE(g_moving_image);
	//poseCost = g_fixed_image.computeSSE(g_moving_image);
	//poseCost = g_fixed_image.computeMSE(g_moving_image);
	//poseCost = g_fixed_image.computeRMSE(g_moving_image);
		
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_LUMINANCE, GL_FLOAT, g_error_image.getRawData());
}








void drawErrorFunction() {
	// Use our shader
	glUseProgram(programID);


	// 1st attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
	   0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
	   3,                  // size
	   GL_FLOAT,           // type
	   GL_FALSE,           // normalized?
	   0,                  // stride
	   (void*)0            // array buffer offset
	);


	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	// Draw the triangles !
	glDrawArrays(GL_TRIANGLES, 0, 6); // Starting from vertex 0; 3 vertices total -> 1 triangle
	

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);


}



//---------
void draw()
//---------
{
    // Clear the buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



    // Display the 3D elements
    glViewport(0, 0, g_main_window_width, g_main_window_height);

    // Store the current transformation matrix
    pushModelViewMatrix();
    pushProjectionMatrix();

    double screen_aspect_ratio(double(g_main_window_width) /
        double(g_main_window_height));

    loadPerspectiveProjectionMatrix(g_initial_fovy, screen_aspect_ratio,
        g_initial_near, g_initial_far);

    loadLookAtModelViewMatrix(0.0, 0.0, g_zoom,
            0.0, 0.0, 0.0,
            0.0, 1.0, 0.0);

	// Update the X-ray image
	updateXRayImage();
	checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

	// Draw the 3D scene
		render();
	checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

	// Restore the current transformation matrix
	popModelViewMatrix();
	popProjectionMatrix();

    // Make sure all the OpenGL code is done
    glFinish();
}


//-----------
void render()
//-----------
{
    try
        {
        // Handle for shader variables
        GLuint handle(0);

        // Enable back face culling
        pushEnableDisableState(GL_CULL_FACE);
        pushEnableDisableState(GL_DEPTH_TEST);
        pushEnableDisableState(GL_BLEND);

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        // Enable the shader
        pushShaderProgram();
        g_display_shader.enable();
        GLint shader_id(g_display_shader.getProgramHandle());



        // Define the light colours here
        GLfloat light_global_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
        GLfloat light_ambient[] = { 0.3, 0.3, 0.3, 1.0 };
        GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
        GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };

        handle = glGetUniformLocation(shader_id, "light_global_ambient");
        glUniform4fv(handle, 1, &light_global_ambient[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "light_ambient");
        glUniform4fv(handle, 1, &light_ambient[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "light_diffuse");
        glUniform4fv(handle, 1, &light_diffuse[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "light_specular");
        glUniform4fv(handle, 1, &light_specular[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        VEC3 light_position(0, 0, g_zoom);
        handle = glGetUniformLocation(shader_id, "light_position");
        glUniform4f(handle,
            light_position.getX(),
            light_position.getY(),
            light_position.getZ(),
            1.0);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Check the status of OpenGL and of the current FBO
        checkFBOErrorStatus(__FILE__, __FUNCTION__, __LINE__);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Store the current transformation matrix
        pushModelViewMatrix();



        // Rotate the sample
        g_current_modelview_matrix *= g_scene_rotation_matrix;

        handle = glGetUniformLocation(shader_id, "g_projection_matrix");
        glUniformMatrix4fv(handle,
            1,
            GL_FALSE,
            g_current_projection_matrix.get());
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "g_modelview_matrix");
        glUniformMatrix4fv(handle,
            1,
            GL_FALSE,
            g_current_modelview_matrix.get());
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        MATRIX4 normal_matrix(g_current_modelview_matrix);
        handle = glGetUniformLocation(shader_id, "g_normal_matrix");
        glUniformMatrix3fv(handle, 1, GL_FALSE, normal_matrix.get3x3());
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        GLint lighting;
        lighting = 1;
        handle = glGetUniformLocation(shader_id,"g_use_lighting");
        glUniform1iv(handle, 1, &lighting);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Store the current transformation matrix
        pushModelViewMatrix();

        // Set the modelview matrix
		g_current_modelview_matrix *= g_rotation_matrix;

        // Display the scenegraph
        g_scene_graph.display(shader_id);

        // Restore the current transformation matrix
        popModelViewMatrix();

        // Define colours
        const GLfloat material_ambient[]  = {0.0, 0.39225,  0.39225,  1.0};
        const GLfloat material_diffuse[]  = {0.0, 0.70754,  0.70754,  1.0};
        const GLfloat material_specular[] = {0.0, 0.708273, 0.708273, 1.0};
        const GLfloat material_shininess = 50.2;

        handle = glGetUniformLocation(shader_id, "material_ambient");
        glUniform4fv(handle, 1, &material_ambient[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "material_diffuse");
        glUniform4fv(handle, 1, &material_diffuse[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "material_specular");
        glUniform4fv(handle, 1, &material_specular[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "material_shininess");
        glUniform1fv(handle, 1, &material_shininess);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Display the source
        g_xray_detector.displaySource();

        
		
		// Display the X-Ray image
        glDisable(GL_CULL_FACE);
        g_xray_renderer.display(true, true, false);





        // Display the beam
        if (g_display_beam)
            {
            const GLfloat material_ambient[]  = {0.75, 0, 0.5, 0.1};
            handle = glGetUniformLocation(shader_id, "material_ambient");
            glUniform4fv(handle, 1, &material_ambient[0]);
            checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

            lighting = 0;
            handle = glGetUniformLocation(shader_id,"g_use_lighting");
            glUniform1iv(handle, 1, &lighting);
            checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

            g_xray_detector.displayBeam();
            }

        // Disable the shader


		
		popShaderProgram();




        // Restore the current transformation matrix
        popModelViewMatrix();

        // Restore the attributes
        popEnableDisableState();
        popEnableDisableState();
        popEnableDisableState();

        // Check the status of OpenGL and of the current FBO
        checkFBOErrorStatus(__FILE__, __FUNCTION__, __LINE__);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);
        }
    // Catch exception if any
    catch (const std::exception& error)
        {
        std::cerr << error.what() << std::endl;
        }



}








//double x[17] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
float computeCostForPose(const double *x, bool leave_final) {

	//multipliers for paramater space
	
	double m[16] =  {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f};



	float cost = -1;


	// save transforms 
	aiMatrix4x4 m1 = g_scene_graph.findNode("MaxScene")->mTransformation;
	aiMatrix4x4 m2 = g_scene_graph.findNode("node-Thu_Meta")->mTransformation;
	aiMatrix4x4 m3 = g_scene_graph.findNode("node-Ind_Meta")->mTransformation;
	aiMatrix4x4 m4 = g_scene_graph.findNode("node-Mid_Meta")->mTransformation;
	aiMatrix4x4 m5 = g_scene_graph.findNode("node-Thi_Meta")->mTransformation;
	aiMatrix4x4 m6 = g_scene_graph.findNode("node-Lit_Meta")->mTransformation;
	aiMatrix4x4 m7 = g_scene_graph.findNode("node-Thu_Prox")->mTransformation;
	aiMatrix4x4 m8 = g_scene_graph.findNode("node-Ind_Midd")->mTransformation;
	aiMatrix4x4 m9 = g_scene_graph.findNode("node-Mid_Midd")->mTransformation;
	aiMatrix4x4 m10 = g_scene_graph.findNode("node-Thi_Midd")->mTransformation;
	aiMatrix4x4 m11 = g_scene_graph.findNode("node-Lit_Midd")->mTransformation;
	/*
	*/

	
	g_scene_graph.rotate("MaxScene", x[0]*m[0], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("MaxScene", x[1]*m[1], VEC3(0.0, 1.0, 0.0));
	g_scene_graph.rotate("MaxScene", x[2]*m[2], VEC3(0.0, 0.0, 1.0));
	g_scene_graph.translate("MaxScene", VEC3(x[3]*m[3], 0.0, 0.0));
	g_scene_graph.translate("MaxScene", VEC3(0.0, x[4]*m[4], 0.0));
	g_scene_graph.translate("MaxScene", VEC3(0.0, 0.0, x[5]*m[5]));

	g_scene_graph.rotate("node-Thu_Meta", x[6]*m[6], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("node-Ind_Meta", x[7]*m[7], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("node-Mid_Meta", x[8]*m[8], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("node-Thi_Meta", x[9]*m[9], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("node-Lit_Meta", x[10]*m[10], VEC3(1.0, 0.0, 0.0));

	g_scene_graph.rotate("node-Thu_Prox", x[11]*m[11], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("node-Ind_Midd", x[12]*m[12], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("node-Mid_Midd", x[13]*m[13], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("node-Thi_Midd", x[14]*m[14], VEC3(1.0, 0.0, 0.0));
	g_scene_graph.rotate("node-Lit_Midd", x[15]*m[15], VEC3(1.0, 0.0, 0.0));
	/*
	*/

	g_scene_graph.updateMatrices();
	g_is_xray_image_up_to_date = false;
	draw();
	updateTexture();
	drawErrorFunction();

	cost = poseCost;

	if(!leave_final) {

		g_scene_graph.findNode("MaxScene")->mTransformation = m1;
		g_scene_graph.findNode("node-Thu_Meta")->mTransformation = m2;
		g_scene_graph.findNode("node-Ind_Meta")->mTransformation = m3;
		g_scene_graph.findNode("node-Mid_Meta")->mTransformation = m4;
		g_scene_graph.findNode("node-Thi_Meta")->mTransformation = m5;
		g_scene_graph.findNode("node-Lit_Meta")->mTransformation = m6;
		g_scene_graph.findNode("node-Thu_Prox")->mTransformation = m7;
		g_scene_graph.findNode("node-Ind_Midd")->mTransformation = m8;
		g_scene_graph.findNode("node-Mid_Midd")->mTransformation = m9;
		g_scene_graph.findNode("node-Thi_Midd")->mTransformation = m10;
		g_scene_graph.findNode("node-Lit_Midd")->mTransformation = m11;

	}

	glfwSwapBuffers(g_p_main_window_id);
	glfwPollEvents();

	return cost;


}



// A cost functor that implements the residual r = 10 - x.
struct CostFunctor {
  bool operator()(const double *x, double* residual) const {
    
	for(int i=0;i<17;i++) {
		printf("%f, ", x[i]);
	}
	printf("\n");

	residual[0] = computeCostForPose(x, false);
    return true;
  }
};




void use_ceres_optimizer() {


  double *x = (double *)malloc(sizeof(double)*18);
    
  for(int ii=0;ii<18;ii++)
	{ x[ii] = 0.0f;}
  
  

  // Build the problem.
  Problem problem;
  // Set up the only cost function (also known as residual). This uses
  // numeric differentiation to obtain the derivative (jacobian).
  CostFunction* cost_function =  new NumericDiffCostFunction<CostFunctor, ceres::RIDDERS, 1, 17> (new CostFunctor);


  problem.AddResidualBlock(cost_function, NULL, x);
  // Run the solver!
  Solver::Options options;
  options.minimizer_type = ceres::TRUST_REGION;
  options.max_num_iterations = 10000;
  //options.function_tolerance = 1e-10;
  //options.gradient_tolerance = 1e10;

  //options.minimizer_progress_to_stdout = true;
  Solver::Summary summary;


  Solve(options, &problem, &summary);

  std::cout << summary.FullReport() << "\n";
//  std::cout << "x : " << initial_x
//            << " -> " << x << "\n";


  computeCostForPose(x, true);


}








//--------------------
int main(int, char **)
//--------------------
{






    try
        {
    	// Create the window and initialise its states
    	initGLFW();
		unsigned char* fixed_image = SOIL_load_image("./xrays/s.png", &width, &height, 0, SOIL_LOAD_RGBA);

		float* p_temp_float = new float[width * height];

		if (!p_temp_float)
		{
			exit(EXIT_FAILURE);
		}
		g_fixed_image = Image<float>(p_temp_float, width, height);

		for (unsigned int i = 0; i < width * height; ++i)
		{
			g_fixed_image.getRawData()[i] = fixed_image[i * 4];
		}
		delete [] p_temp_float;

		g_fixed_image = g_fixed_image.normalized();


		// Normalise the fixed image between 0 and 1
		//g_fixed_image = g_fixed_image.normalised();

		// More robust normalization
		g_fixed_image -= g_fixed_image.getAverage();
		g_fixed_image /= g_fixed_image.getStandardDeviation();


		g_fixed_image.save("fixed_image.mha");

		// Initialise GLEW
		initialiseGLEW();
		checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

		// Check the OpenGL version
		std::cout << "GL:\t" << glGetString(GL_VERSION) << std::endl;

		// Check the GLSL version
		std::cout << "GLSL:\t" <<
			glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

		// Initialise OpenGL
		initGL();
		checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

		// Check the current FBO
		checkFBOErrorStatus(__FILE__, __FUNCTION__, __LINE__);


		// Load the data
		loadDetector();
		loadSource();
		loadXRaySimulator();

		// Add the geometry to the X-ray renderer
		loadSceneGraph();

		setup_error_texture();

		
		programID = LoadShaders( "TransformVertexShader.vertexshader", "TextureFragmentShader.fragmentshader" );


		g_scene_graph.updateMatrices();
		

		use_ceres_optimizer();	

		

		// Launch the event loop
		
		while (!glfwWindowShouldClose(g_p_main_window_id))
			{
				g_scene_graph.updateMatrices();
				g_is_xray_image_up_to_date = false;

				draw();

				updateTexture();
				drawErrorFunction();

				// Swap front and back buffers
				glfwSwapBuffers(g_p_main_window_id);

				// Poll for and process events
				glfwPollEvents();

			}
		/**/
		}
	// Catch exception if any
	catch (const std::exception& error)
		{
		std::cerr << error.what() << std::endl;
		}
	catch (const std::string& error)
		{
		std::cerr << error << std::endl;
		}
	catch (const char* error)
		{
		std::cerr << error << std::endl;
		}

	// Close the window and shut GLFW
	if (g_p_main_window_id)
		{
		glfwDestroyWindow(g_p_main_window_id);
		g_p_main_window_id = 0;
		glfwTerminate();
		}

	// Return an exit code
	return (EXIT_SUCCESS);
}
