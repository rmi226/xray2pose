#ifdef HAS_GLEW
#include <GL/glew.h>
#endif

#ifndef GVXR_CONFIG_H
#include "gVirtualXRay/gVirtualXRayConfig.h"
#endif

#ifndef __SceneGraphBinder_h
#include "SceneGraphBinder.h"
#endif

// For types such as VEC3, MATRIX4
#ifndef __Types_h
#include "gVirtualXRay/Types.h"
#endif

// To initialise GLEW, to define matrix stacks, to define state stacks, etc.
#ifndef __OpenGLUtilities_h
#include "gVirtualXRay/OpenGLUtilities.h"
#endif


SceneGraphBinder::SceneGraphBinder()
{}


SceneGraphBinder::SceneGraphBinder(const SceneGraphBinder& aSceneGraph)
{
}


SceneGraphBinder::~SceneGraphBinder()
{}


SceneGraphBinder& SceneGraphBinder::operator=(const SceneGraphBinder& aSceneGraph)
{
	return (*this);
}


const std::vector<gVirtualXRay::PolygonMesh>& SceneGraphBinder::getPolygonMeshSet() const
{
	return (m_p_polygon_mesh_set);
}


std::vector<gVirtualXRay::PolygonMesh>& SceneGraphBinder::getPolygonMeshSet()
{
	return (m_p_polygon_mesh_set);
}


void SceneGraphBinder::scale(const std::string& aNodeLabel,
		const gVirtualXRay::VEC3& aScalingFactorSet)
{
	scale(aNodeLabel.data(), aScalingFactorSet);
}


//--------------------------------------------------------------------
void SceneGraphBinder::rotate(const std::string& aNodeLabel,
		                      float aRotationAngleInDegrees,
							  const gVirtualXRay::VEC3& aRotationAxis)
//--------------------------------------------------------------------
{
	rotate(aNodeLabel.data(), aRotationAngleInDegrees, aRotationAxis);
}


gVirtualXRay::MATRIX4 SceneGraphBinder::getTransformationMatrix(const std::string& aNodeLabel) const
{
	return (getTransformationMatrix(aNodeLabel.data()));
}


void SceneGraphBinder::display(GLint aShaderID)
{
	for (std::vector<gVirtualXRay::PolygonMesh>::iterator ite = getPolygonMeshSet().begin();
			ite != getPolygonMeshSet().end();
			++ite)
	{
		// Store the current transformation matrix
		gVirtualXRay::pushModelViewMatrix();

		
		ite->setHounsfieldValue(100.0f);


		gVirtualXRay::g_current_modelview_matrix *= ite->getTransformationMatrix();

		gVirtualXRay::applyModelViewMatrix();

		// Get the material
		gVirtualXRay::Material& material(ite->getMaterial());

		// Define colours inshader
		GLuint handle(glGetUniformLocation(aShaderID, "material_ambient"));
		glUniform4fv(handle, 1, &(material.getAmbientColour()[0]));
		gVirtualXRay::checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

		handle = glGetUniformLocation(aShaderID, "material_diffuse");
		glUniform4fv(handle, 1, &(material.getDiffuseColour()[0]));
		gVirtualXRay::checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

		handle = glGetUniformLocation(aShaderID, "material_specular");
		glUniform4fv(handle, 1, &(material.getSpecularColour()[0]));
		gVirtualXRay::checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

		handle = glGetUniformLocation(aShaderID, "material_shininess");
		glUniform1f(handle, material.getShininess());
		gVirtualXRay::checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

		ite->display();

		// Restore the current transformation matrix
		gVirtualXRay::popModelViewMatrix();
	}
}
