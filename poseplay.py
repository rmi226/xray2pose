#! /usr/bin/env python3

# For plotting with PIL
from PIL import Image

# For plotting with matplotlib
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

from matplotlib import animation, rc
from IPython.display import HTML

from scipy.optimize import minimize
import numpy as np

import gvxrPython3 as gvxr
import scipy.io


skel = '4123'
im_filename = 'data/'+skel+'.jpg'
shape_filename = 'data/'+skel+'_GT.mat'



# Create an OpenGL context
print("Create an OpenGL context")
gvxr.createWindow();
gvxr.setWindowSize(512, 512);


# Set up the beam
print("Set up the beam")
gvxr.setSourcePosition(40.0,  0.0, 0.0, "cm");
gvxr.usePointSource();
#gvxr.useParallelBeam();
gvxr.setMonoChromatic(0.08, "MeV", 1000);

# Set up the detector
print("Set up the detector");
gvxr.setDetectorPosition(-2.0, 0.0, 0.0, "cm");

gvxr.setDetectorUpVector(0, 1, 0);

#gvxr.setDetectorNumberOfPixels(np_normalized_fixed_image.shape[1], np_normalized_fixed_image.shape[0]);

gvxr.setDetectorNumberOfPixels(600, 600);


gvxr.setDetectorPixelSize(1.5, 1.5, "mm");

# Load the data and display before transformation from PDM
print("Load the data");
gvxr.loadSceneGraph("handstraight_v1.DAE", "mm");

print ("Number of meshes in scenegraph:\t" + str(len(gvxr.getMeshLabelSet())))
for (idx, mesh) in enumerate(gvxr.getMeshLabelSet()):
    #print("Set the mesh's Hounsfield unit");
    print(mesh)
    gvxr.setHU(mesh, 1000)
    
gvxr.moveToCentre();
#
gvxr.scaleScene(150, 150, 150, "cm");
gvxr.rotateScene(180, 0, 1, 0);

#gvxr.rotateNode("node-Mid_Dist", -45, 0.0, 1.0, 0.0);
#gvxr.setNodeTransformationMatrix(mesh, unmove)
np_image = gvxr.computeXRayImage();
plt.imshow(np_image)


# Load PDM and compute transform angles 

hand_shape = scipy.io.loadmat(shape_filename)



#X = hand_shape['wholeHand'][:, 0] / np.max(hand_shape['wholeHand'][:, 0]) * 300
#Y = 300 - hand_shape['wholeHand'][:, 1] / np.max(hand_shape['wholeHand'][:, 1]) * 300

X = hand_shape['wholeHand'][:, 0]# / np.max(hand_shape['wholeHand'][:, 0])
Y = -hand_shape['wholeHand'][:, 1]# / np.max(hand_shape['wholeHand'][:, 1]) 



# Compute long bone angles w.r.t. y-axis

an_Lit_Meta = np.degrees(np.arctan2(Y[1]-Y[0], X[1]-X[0])) - 90
an_Lit_Prox = np.degrees(np.arctan2(Y[3]-Y[2], X[3]-X[2])) - 90 
an_Lit_Midd = np.degrees(np.arctan2(Y[5]-Y[4], X[5]-X[4])) - 90 
an_Lit_Dist = np.degrees(np.arctan2(Y[7]-Y[6], X[7]-X[6])) - 90 

an_Thi_Meta = np.degrees(np.arctan2(Y[9]-Y[8], X[9]-X[8])) - 90
an_Thi_Prox = np.degrees(np.arctan2(Y[11]-Y[10], X[11]-X[10])) - 90 
an_Thi_Midd = np.degrees(np.arctan2(Y[13]-Y[12], X[13]-X[12])) - 90 
an_Thi_Dist = np.degrees(np.arctan2(Y[15]-Y[14], X[15]-X[14])) - 90 

an_Mid_Meta = np.degrees(np.arctan2(Y[17]-Y[16], X[17]-X[16])) - 90
an_Mid_Prox = np.degrees(np.arctan2(Y[19]-Y[18], X[19]-X[18])) - 90 
an_Mid_Midd = np.degrees(np.arctan2(Y[21]-Y[20], X[21]-X[20])) - 90 
an_Mid_Dist = np.degrees(np.arctan2(Y[23]-Y[22], X[23]-X[22])) - 90 

an_Ind_Meta = np.degrees(np.arctan2(Y[25]-Y[24], X[25]-X[24])) - 90
an_Ind_Prox = np.degrees(np.arctan2(Y[27]-Y[26], X[27]-X[26])) - 90 
an_Ind_Midd = np.degrees(np.arctan2(Y[29]-Y[28], X[29]-X[28])) - 90 
an_Ind_Dist = np.degrees(np.arctan2(Y[31]-Y[30], X[31]-X[30])) - 90 

an_Thu_Meta = np.degrees(np.arctan2(Y[33]-Y[32], X[33]-X[32])) - 90
an_Thu_Prox = np.degrees(np.arctan2(Y[35]-Y[34], X[35]-X[34])) - 90 
an_Thu_Dist = np.degrees(np.arctan2(Y[37]-Y[36], X[37]-X[36])) - 90 

# apply transforms 

gvxr.rotateNode("Lit_Meta", an_Lit_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Lit_Prox", an_Lit_Prox - an_Lit_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Lit_Midd", an_Lit_Midd - an_Lit_Prox, 1.0, 0.0, 0.0);
gvxr.rotateNode("Lit_Dist", an_Lit_Dist - an_Lit_Midd, 1.0, 0.0, 0.0);

gvxr.rotateNode("Thi_Meta", an_Thi_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Thi_Prox", an_Thi_Prox - an_Thi_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Thi_Midd", an_Thi_Midd - an_Thi_Prox, 1.0, 0.0, 0.0);
gvxr.rotateNode("Thi_Dist", an_Thi_Dist - an_Thi_Midd, 1.0, 0.0, 0.0);

gvxr.rotateNode("Mid_Meta", an_Mid_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Mid_Prox", an_Mid_Prox - an_Mid_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Mid_Midd", an_Mid_Midd - an_Mid_Prox, 1.0, 0.0, 0.0);
gvxr.rotateNode("Mid_Dist", an_Mid_Dist - an_Mid_Midd, 1.0, 0.0, 0.0);

gvxr.rotateNode("Ind_Meta", an_Ind_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Ind_Prox", an_Ind_Prox - an_Ind_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Ind_Midd", an_Ind_Midd - an_Ind_Prox, 1.0, 0.0, 0.0);
gvxr.rotateNode("Ind_Dist", an_Ind_Dist - an_Ind_Midd, 1.0, 0.0, 0.0);

gvxr.rotateNode("Thu_Meta", an_Thu_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Thu_Prox", an_Thu_Prox - an_Thu_Meta, 1.0, 0.0, 0.0);
gvxr.rotateNode("Thu_Dist", an_Thu_Dist - an_Thu_Prox, 1.0, 0.0, 0.0);

np_image = gvxr.computeXRayImage();
plt.figure()
plt.imshow(np_image)
plt.draw()





image = Image.open(im_filename).convert('L');
np_fixed_image = np.array(image).astype("float")
np_fixed_image = scipy.misc.imresize(np_fixed_image, [600, 480])


plt.figure()
plt.hold(True)
plt.imshow(np_fixed_image)
plt.scatter(X, -Y, zorder=2, c=[1, 0, 0])
plt.draw()









