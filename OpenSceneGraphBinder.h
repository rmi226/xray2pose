#ifndef __OpenSceneGraphBinder_h
#define __OpenSceneGraphBinder_h

#include <osg/Node>
#include <osg/Group>
#include <osg/MatrixTransform>

#ifndef __GeodeFinder_h
#include "GeodeFinder.h"
#endif

#ifndef __SceneGraphBinder_h
#include "SceneGraphBinder.h"
#endif


class OpenSceneGraphBinder: public SceneGraphBinder
{
public:
	OpenSceneGraphBinder();
	OpenSceneGraphBinder(const OpenSceneGraphBinder& aSceneGraph);
	OpenSceneGraphBinder(osg::Group* apSceneGraph);
	virtual ~OpenSceneGraphBinder();

	OpenSceneGraphBinder& operator=(const OpenSceneGraphBinder& aSceneGraph);


	void setSceneGraph(osg::Group* apSceneGraph);


	virtual void updateMatrices();


	virtual void scale(const char* aNodeLabel,
			const gVirtualXRay::VEC3& aScalingFactorSet);


	virtual void rotate(const char* aNodeLabel,
			float aRotationAngleInDegrees,
			const gVirtualXRay::VEC3& aRotationAxis);


	virtual gVirtualXRay::MATRIX4 getTransformationMatrix(const char* aNodeLabel) const;


	osg::Node* findNode(const char* aNodeLabel);
	const osg::Node* findNode(const char* aNodeLabel) const;


private:
	osg::Group* m_p_scene_graph;
	GeodeFinder m_geode_finder;
};


#endif // __OpenSceneGraphBinder_h
