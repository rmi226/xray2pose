#ifndef __AssimpSceneGraphBinder_h
#define __AssimpSceneGraphBinder_h

#include <string>

// ASSIMP to load Collada files
#include <assimp/scene.h>
#include <assimp/matrix4x4.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>


#ifndef __SceneGraphBinder_h
#include "SceneGraphBinder.h"
#endif


class AssimpSceneGraphBinder: public SceneGraphBinder
{
public:
	AssimpSceneGraphBinder();
	AssimpSceneGraphBinder(const AssimpSceneGraphBinder& aSceneGraph);
	virtual ~AssimpSceneGraphBinder();

	AssimpSceneGraphBinder& operator=(const AssimpSceneGraphBinder& aSceneGraph);


	void setSceneGraph(const aiScene* apSceneGraph);


	void loadSceneGraph(const char* aFileName);


	void loadSceneGraph(const std::string& aFileName);


	void updateMatrices();



	virtual void scale(const char* aNodeLabel,
			const gVirtualXRay::VEC3& aScalingFactorSet);


	virtual void rotate(const char* aNodeLabel,
			float aRotationAngleInDegrees,
			const gVirtualXRay::VEC3& aRotationAxis);


	virtual void translate(const char* aNodeLabel,
							        const gVirtualXRay::VEC3& aTranslationAmount);

	virtual gVirtualXRay::MATRIX4 getTransformationMatrix(const char* aNodeLabel) const;


	const aiNode* findNode(const char* aNodeLabel) const;
	const aiNode* findNode(const std::string& aNodeLabel) const;

	aiNode* findNode(const char* aNodeLabel);
	aiNode* findNode(const std::string& aNodeLabel);

private:

	void recursivePolygonMeshBuild (const aiNode* nd, aiMatrix4x4 tStack, int& anIndex);

	void recursiveUpdateMatrices (const aiNode* nd, aiMatrix4x4 tStack, int& anIndex);


	const aiNode* recursiveFindNode(const aiNode* nd, const char* aNodeLabel) const;
	aiNode* recursiveFindNode(aiNode* nd, const char* aNodeLabel);



	Assimp::Importer m_importer;
	aiScene* m_p_scene;
};


#endif // __AssimpSceneGraphBinder_h
