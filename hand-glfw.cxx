/*

Copyright (c) 2016, Dr Franck P. Vidal (franck.p.vidal@fpvidal.net),
http://www.fpvidal.net/
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the Bangor University nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


/**
********************************************************************************
*
*   @file       hand-glfw.cxx
*
*   @brief      Hand demo.
*
*   @version    1.0
*
*   @date       05/07/2018
*
*   @author     Dr Franck P. Vidal
*
*   @section    License
*               BSD 3-Clause License.
*
*               For details on use and redistribution please refer
*               to http://opensource.org/licenses/BSD-3-Clause
*
*   @section    Copyright
*               (c) by Dr Franck P. Vidal (franck.p.vidal@fpvidal.net),
*               http://www.fpvidal.net/, Jul 2018, 2018, version 1.0,
*               BSD 3-Clause License
*
********************************************************************************
*/


//******************************************************************************
//  Include
//******************************************************************************
#ifdef HAS_GLEW
#include <GL/glew.h>
#endif

#ifndef GVXR_CONFIG_H
#include "gVirtualXRay/gVirtualXRayConfig.h"
#endif

// To easily create file names
#include <sstream>

// To save raw data into a file
#include <cstdio>

// ASSIMP to load Collada files
#include <assimp/scene.h>
#include <assimp/matrix4x4.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>   

#define aisgl_min(x,y) (x<y?x:y)
#define aisgl_max(x,y) (y>x?y:x)

// GLFW to create an OpenGL window
#define GLFW_INCLUDE_GLCOREARB 1

#include <GLFW/glfw3.h>

// For VEC3, MATRIX4, ...
#ifndef GVXR_TYPES_H
#include "gVirtualXRay/Types.h"
#endif

// To defined the energy of the incident beam
#ifndef GVXR_XRAY_BEAM_H
#include "gVirtualXRay/XRayBeam.h"
#endif

// For rendering the X-ray image
#ifndef GVXR_XRAY_RENDERER_H
#include "gVirtualXRay/XRayRenderer.h"
#endif

#include "display_gl2.frag.h"
#include "display_gl2.vert.h"

#include "display_gl3.frag.h"
#include "display_gl3.vert.h"


//******************************************************************************
//  Name space
//******************************************************************************
using namespace gVirtualXRay;
using namespace std;


//******************************************************************************
//  Defines
//******************************************************************************
#define PREFIX ".."


//******************************************************************************
//  Constant variables
//******************************************************************************
const GLfloat g_rotation_speed(2.0);

const float g_incident_energy(0.08 * MeV);
//const float g_scaling_factor(0.6);
const float g_scaling_factor(2.0);
const VEC2 g_resolution(1.0 * g_scaling_factor * mm, 1 * g_scaling_factor * mm);
const Vec2ui g_number_of_pixels(500 / g_scaling_factor, 500 / g_scaling_factor);

const double g_initial_fovy(45);        // Field of view along the y-axis
const double g_initial_near(5.0 * cm);  // Near clipping plane
const double g_initial_far(500.0 * cm); // Far clipping plane

const VEC3 g_initial_source_position(   0.0,  50.0 * cm, 0.0);
const VEC3 g_initial_detector_position( 0.0, -25.0 * cm, 0.0);
const VEC3 g_initial_detector_up_vector(0.0, 0.0, 1.0);
const VEC3 g_background_colour(0.5, 0.5, 0.5);

const unsigned char HARD_BONE_ID(0);

const double g_p_HU_set [] =
{
        1000,
};


//******************************************************************************
//  Global variables
//******************************************************************************
GLsizei g_main_window_width( 600);
GLsizei g_main_window_height(600);
GLFWwindow* g_p_main_window_id(0);
GLfloat g_zoom(100.0 * cm);

int g_button(-1);
int g_button_state(-1);
bool g_use_arc_ball(false);
GLint g_last_x_position(0);
GLint g_last_y_position(0);
GLint g_current_x_position(0);
GLint g_current_y_position(0);

bool g_display_beam(true);

Matrix4x4<GLfloat> g_scene_rotation_matrix;
Matrix4x4<GLfloat> g_rotation_matrix;
VEC3 g_translation_vector;

std::vector<PolygonMesh> g_p_polygon_mesh_set(1);

XRayBeam g_xray_beam;
XRayDetector g_xray_detector;
XRayRenderer g_xray_renderer;
Shader g_display_shader;

bool g_is_xray_image_up_to_date(false);
bool g_display_wireframe(false);
bool g_display_outside_polygon(true);


// my additions

/// Create an instance of the Importer class
Assimp::Importer importer;
float scaleFactor = 1.0f;
/* the global Assimp scene object */
const aiScene* scene = NULL;
GLuint scene_list = 0;
aiVector3D scene_min, scene_max, scene_center;


// Geometric data
vector<double> g_p_vertex_set_1;
vector<unsigned char> g_p_index_set_1;
vector<float> g_p_vertex_set_2;





//******************************************************************************
//  Function declaration
//******************************************************************************
void initGL();

void framebufferSizeCallback(GLFWwindow* apWindow, int aWidth, int aHeight);

void keyCallback(GLFWwindow* apWindow,
    int aKey,
    int aScanCode,
    int anAction,
    int aModifierKey);

void mouseButtonCallback(GLFWwindow* apWindow,
    int aButton,
    int aButtonState,
    int aModifierKey);

void cursorPosCallback(GLFWwindow* apWindow, double x, double y);

void scrollCallback(GLFWwindow* apWindow, double xoffset, double yoffset);

void draw();

void render();

void quit();

void loadSTLFile();


bool Import3DFromFile( const std::string& pFile);

void loadDetector();

void loadSource();

void loadXRaySimulator();

Vec3<GLfloat> getArcballVector(int x, int y);

void computeRotation(MATRIX4& aRotationMatrix);

void updateXRayImage();

void errorCallback(int error, const char* description);


//------------------------
inline int ipart(double X)
//------------------------
{
    return (int(X));
}


//---------------------------
inline double fpart(double X)
//---------------------------
{
    return (double(X) - double(ipart(X)));
}


//----------------------------
inline double rfpart(double X)
//----------------------------
{
    return (1.0 - fpart(X));
}


//------------------------------
inline void swap(int& a, int& b)
//------------------------------
{
    int temp(b);
    b = a;
    a = temp;
}


//-------------------------------
inline void plot(int x,
                 int y,
                 float distance,
                 int width,
                 int height,
                 float* apData,
                 float intensity)
//-------------------------------
{
    int index(x + y * width);

    if (index >= 0 && index < width * height)
        {
        apData[x + y * width] += fabs(distance * intensity);
        }
}


//-----------------------------
int main(int argc, char** argv)
//-----------------------------
{
    
    
    Import3DFromFile("./hand.dae");
	
    
    try
        {
        // Initialize random seed
        srand(time(0));

        // Set an error callback
        glfwSetErrorCallback(errorCallback);

        // Register the exit callback
        atexit(quit);

        // Initialize GLFW
        if (!glfwInit())
            {
            throw Exception(__FILE__, __FUNCTION__, __LINE__,
                "ERROR: cannot initialise GLFW.");
            }

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

        // Enable anti-aliasing
        glfwWindowHint(GLFW_SAMPLES, 4);

        // Create a windowed mode window and its OpenGL context
        g_p_main_window_id = glfwCreateWindow(g_main_window_width,
            g_main_window_height,
            "gVirtualXRay -- Hand Demo -- GLFW3",
            0,
            0);
        
        // Window cannot be created
        if (!g_p_main_window_id)
            {
            glfwTerminate();
            throw Exception(__FILE__, __FUNCTION__, __LINE__,
                "ERROR: cannot create a GLFW windowed mode window and "
                "its OpenGL context.");
            }

        // Make the window's context current
        glfwMakeContextCurrent(g_p_main_window_id);

        // Initialise GLEW
        initialiseGLEW();
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Check the OpenGL version
        std::cout << "GL:\t" << glGetString(GL_VERSION) << std::endl;

        // Check the GLSL version
        std::cout << "GLSL:\t" <<
            glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

        // Initialise OpenGL
        initGL();
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Check the current FBO
        checkFBOErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Initialize GLFL callback
        glfwSetKeyCallback(g_p_main_window_id, keyCallback);
        glfwSetMouseButtonCallback(g_p_main_window_id, mouseButtonCallback);
        glfwSetCursorPosCallback(g_p_main_window_id, cursorPosCallback);
        glfwSetScrollCallback(g_p_main_window_id, scrollCallback);
        glfwSetFramebufferSizeCallback(g_p_main_window_id,
            framebufferSizeCallback);

        // Load the data
        loadDetector();
        loadSource();
        loadXRaySimulator();

        // Add the geometry to the X-ray renderer
        loadSTLFile();

        // Set the projection matrix
        GLint width(0);
        GLint height(0);
        glfwGetFramebufferSize(g_p_main_window_id, &width, &height);
        framebufferSizeCallback(g_p_main_window_id, width, height);

        g_scene_rotation_matrix *= Matrix4x4<GLfloat>::buildRotationMatrix(
            180.0, VEC3(0, 0, 1));
        g_scene_rotation_matrix *= Matrix4x4<GLfloat>::buildRotationMatrix(
             60.0, VEC3(1, 0, 0));

        // Launch the event loop
        while (!glfwWindowShouldClose(g_p_main_window_id))
            {
            draw();

            // Swap front and back buffers
            glfwSwapBuffers(g_p_main_window_id);

            // Poll for and process events
            glfwPollEvents();
            }
        }
    // Catch exception if any
    catch (const std::exception& error)
        {
        std::cerr << error.what() << std::endl;
        }
    catch (const std::string& error)
        {
        std::cerr << error << std::endl;
        }
    catch (const char* error)
        {
        std::cerr << error << std::endl;
        }

    // Close the window and shut GLFW
    if (g_p_main_window_id)
        {
        glfwDestroyWindow(g_p_main_window_id);
        g_p_main_window_id = 0;
        glfwTerminate();
        }

    // Return an exit code
    return (EXIT_SUCCESS);
}


//----------------------------------------------------
void errorCallback(int error, const char* description)
//----------------------------------------------------
{
    std::cerr << "GLFW error: " << description << std::endl;
}


//---------
void quit()
//---------
{
    if (g_p_main_window_id)
        {
        glfwDestroyWindow(g_p_main_window_id);
        g_p_main_window_id = 0;
        glfwTerminate();
        }
}


//-----------
void initGL()
//-----------
{
    std::cout << "OpenGL renderer:   " <<
        glGetString(GL_RENDERER)   << std::endl;
    
    std::cout << "OpenGL version:    " <<
        glGetString(GL_VERSION)    << std::endl;
    
    std::cout << "OpenGL vender:     " <<
        glGetString(GL_VENDOR)     << std::endl;
    
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    // Enable the Z-buffer
    glEnable(GL_DEPTH_TEST);
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    // Set the background colour
    glClearColor(g_background_colour.getX(),
        g_background_colour.getY(),
        g_background_colour.getZ(),
        1.0);
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    glEnable(GL_MULTISAMPLE);
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    // Initialise the shaders
    char* p_vertex_shader(0);
    char* p_fragment_shader(0);

    int z_lib_return_code_vertex(0);
    int z_lib_return_code_fragment(0);

    std::string vertex_shader;
    std::string fragment_shader;

    // Display shader
    if (useOpenGL3_2OrAbove())
        {
        z_lib_return_code_vertex   = inflate(g_display_gl3_vert,
            sizeof(g_display_gl3_vert),
            &p_vertex_shader);
        
        z_lib_return_code_fragment = inflate(g_display_gl3_frag,
            sizeof(g_display_gl3_frag),
            &p_fragment_shader);
        
        g_display_shader.setLabels("display_gl3.vert", "display_gl3.frag");
        }
    else
        {
        z_lib_return_code_vertex   = inflate(g_display_gl2_vert,
            sizeof(g_display_gl2_vert),
            &p_vertex_shader);
        
        z_lib_return_code_fragment = inflate(g_display_gl2_frag,
            sizeof(g_display_gl2_frag),
            &p_fragment_shader);
        
        g_display_shader.setLabels("display_gl2.vert", "display_gl2.frag");
        }

    vertex_shader   = p_vertex_shader;
    fragment_shader = p_fragment_shader;
    
    delete [] p_vertex_shader;
    delete [] p_fragment_shader;
    
    p_vertex_shader = 0;
    p_fragment_shader = 0;
    
    if (z_lib_return_code_vertex <= 0 || z_lib_return_code_fragment <= 0 ||
            !vertex_shader.size() || !fragment_shader.size())
        {
        throw Exception(__FILE__, __FUNCTION__, __LINE__,
            "Cannot decode the shader using ZLib.");
        }
    g_display_shader.loadSource(vertex_shader, fragment_shader);
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);
}


//------------------------------------------------------
void displayPatient(std::vector<PolygonMesh>& apMeshSet,
                    GLint aShaderID)
//------------------------------------------------------
{
    for (unsigned int i(1); i < apMeshSet.size(); ++i)
    {
        // Get the material
        Material& material(apMeshSet[i].getMaterial());

        // Define colours inshader
        GLuint handle(glGetUniformLocation(aShaderID, "material_ambient"));
        glUniform4fv(handle, 1, &(material.getAmbientColour()[0]));
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(aShaderID, "material_diffuse");
        glUniform4fv(handle, 1, &(material.getDiffuseColour()[0]));
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(aShaderID, "material_specular");
        glUniform4fv(handle, 1, &(material.getSpecularColour()[0]));
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(aShaderID, "material_shininess");
        glUniform1f(handle, material.getShininess());
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        if (g_display_wireframe)
        {
            apMeshSet[i].displayWireFrame();
        }
        else
        {
            apMeshSet[i].display();
        }
    }

    // Display the skin with transparency
    if (g_display_outside_polygon)
    {
        if (!g_display_wireframe)
        {
            pushEnableDisableState(GL_BLEND);

            glEnable (GL_BLEND);
            glBlendFunc (GL_ONE, GL_ONE);

            apMeshSet[HARD_BONE_ID].display();

            popEnableDisableState();
        }
        else
        {
            apMeshSet[HARD_BONE_ID].displayWireFrame();
        }
    }
}


//---------
void draw()
//---------
{
    // Clear the buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Display the 3D elements
    glViewport(0, 0, g_main_window_width, g_main_window_height);

    // Store the current transformation matrix
    pushModelViewMatrix();
    pushProjectionMatrix();

    double screen_aspect_ratio(double(g_main_window_width) /
        double(g_main_window_height));

    loadPerspectiveProjectionMatrix(g_initial_fovy, screen_aspect_ratio,
        g_initial_near, g_initial_far);

    loadLookAtModelViewMatrix(0.0, 0.0, g_zoom,
            0.0, 0.0, 0.0,
            0.0, 1.0, 0.0);

    // Update the X-ray image
    updateXRayImage();
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    // Draw the 3D scene
    render();
    checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

    // Restore the current transformation matrix
    popModelViewMatrix();
    popProjectionMatrix();

    // Make sure all the OpenGL code is done
    glFinish();
}


//-----------
void render()
//-----------
{
    try
        {
        // Handle for shader variables
        GLuint handle(0);

        // Enable back face culling
        pushEnableDisableState(GL_CULL_FACE);
        pushEnableDisableState(GL_DEPTH_TEST);
        pushEnableDisableState(GL_BLEND);

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        // Enable the shader
        pushShaderProgram();
        g_display_shader.enable();
        GLint shader_id(g_display_shader.getProgramHandle());

        // Define the light colours here
        GLfloat light_global_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
        GLfloat light_ambient[] = { 0.3, 0.3, 0.3, 1.0 };
        GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
        GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };

        handle = glGetUniformLocation(shader_id, "light_global_ambient");
        glUniform4fv(handle, 1, &light_global_ambient[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "light_ambient");
        glUniform4fv(handle, 1, &light_ambient[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "light_diffuse");
        glUniform4fv(handle, 1, &light_diffuse[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "light_specular");
        glUniform4fv(handle, 1, &light_specular[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        VEC3 light_position(0, 0, g_zoom);
        handle = glGetUniformLocation(shader_id, "light_position");
        glUniform4f(handle,
            light_position.getX(),
            light_position.getY(),
            light_position.getZ(),
            1.0);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Check the status of OpenGL and of the current FBO
        checkFBOErrorStatus(__FILE__, __FUNCTION__, __LINE__);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Store the current transformation matrix
        pushModelViewMatrix();

        // Rotate the sample
        g_current_modelview_matrix *= g_scene_rotation_matrix;

        handle = glGetUniformLocation(shader_id, "g_projection_matrix");
        glUniformMatrix4fv(handle,
            1,
            GL_FALSE,
            g_current_projection_matrix.get());
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "g_modelview_matrix");
        glUniformMatrix4fv(handle,
            1,
            GL_FALSE,
            g_current_modelview_matrix.get());
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        MATRIX4 normal_matrix(g_current_modelview_matrix);
        handle = glGetUniformLocation(shader_id, "g_normal_matrix");
        glUniformMatrix3fv(handle, 1, GL_FALSE, normal_matrix.get3x3());
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        GLint lighting;
        lighting = 1;
        handle = glGetUniformLocation(shader_id,"g_use_lighting");
        glUniform1iv(handle, 1, &lighting);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Store the current transformation matrix
        pushModelViewMatrix();

        // Set the modelview matrix 
        g_current_modelview_matrix *= g_rotation_matrix;

        applyModelViewMatrix();
        // Display the current patient
	displayPatient(g_p_polygon_mesh_set, shader_id);
        // Restore the current transformation matrix
        popModelViewMatrix();

        // Define colours
        const GLfloat material_ambient[]  = {0.0, 0.39225,  0.39225,  1.0};
        const GLfloat material_diffuse[]  = {0.0, 0.70754,  0.70754,  1.0};
        const GLfloat material_specular[] = {0.0, 0.708273, 0.708273, 1.0};
        const GLfloat material_shininess = 50.2;

        handle = glGetUniformLocation(shader_id, "material_ambient");
        glUniform4fv(handle, 1, &material_ambient[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "material_diffuse");
        glUniform4fv(handle, 1, &material_diffuse[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "material_specular");
        glUniform4fv(handle, 1, &material_specular[0]);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        handle = glGetUniformLocation(shader_id, "material_shininess");
        glUniform1fv(handle, 1, &material_shininess);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

        // Display the source
        g_xray_detector.displaySource();

        // Display the X-Ray image
        glDisable(GL_CULL_FACE);
        g_xray_renderer.display(true, true, false);

        // Display the beam
        if (g_display_beam)
            {
            const GLfloat material_ambient[]  = {0.75, 0, 0.5, 0.1};
            handle = glGetUniformLocation(shader_id, "material_ambient");
            glUniform4fv(handle, 1, &material_ambient[0]);
            checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

            lighting = 0;
            handle = glGetUniformLocation(shader_id,"g_use_lighting");
            glUniform1iv(handle, 1, &lighting);
            checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);

            g_xray_detector.displayBeam();
            }

        // Disable the shader
        popShaderProgram();

        // Restore the current transformation matrix
        popModelViewMatrix();

        // Restore the attributes
        popEnableDisableState();
        popEnableDisableState();
        popEnableDisableState();

        // Check the status of OpenGL and of the current FBO
        checkFBOErrorStatus(__FILE__, __FUNCTION__, __LINE__);
        checkOpenGLErrorStatus(__FILE__, __FUNCTION__, __LINE__);
        }
    // Catch exception if any
    catch (const std::exception& error)
        {
        std::cerr << error.what() << std::endl;
        }
}


//-------------------------------------------------------------------------------
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
//-------------------------------------------------------------------------------
{
    if (action == GLFW_PRESS)
        {
        switch (key)
            {
            case GLFW_KEY_S:
                {
                    updateXRayImage();
                    const XRayRenderer::PixelType* raw_data = g_xray_renderer.getEnergyFluenceRawData();

                    stringstream file_name;
                    file_name << "energy_fluence_" << g_number_of_pixels[0] << "x" << g_number_of_pixels[1] << ".raw";
                    
                    FILE* p_file_descriptor = fopen(file_name.str().data(), "wb");
                    if (p_file_descriptor)
                    {
                        fwrite(raw_data, 
                                sizeof(XRayRenderer::PixelType), 
                                g_number_of_pixels[0] * g_number_of_pixels[1], 
                                p_file_descriptor);
                        
                        fclose(p_file_descriptor);
                    }
                }
                break;


            case GLFW_KEY_A:
                // Enable/Disable artefact filtering
                g_xray_renderer.useArtifactFiltering(g_xray_renderer.useArtifactFiltering());

                // The X-ray image is not up-to-date
                g_is_xray_image_up_to_date = false;
                break;

            case GLFW_KEY_Q:
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(g_p_main_window_id, GL_TRUE);
                break;

            case GLFW_KEY_B:
                g_display_beam = !g_display_beam;
                break;

            case GLFW_KEY_N:
                g_xray_renderer.useNegativeFilteringFlag(
                    !g_xray_renderer.getNegativeFilteringFlag());
                break;

            case GLFW_KEY_W:
                g_display_wireframe = !g_display_wireframe;
                break;

            case GLFW_KEY_V:
                g_display_outside_polygon = !g_display_outside_polygon;
                break;

            case GLFW_KEY_1:
                // The X-ray image is not up-to-date
                g_is_xray_image_up_to_date = false;
                // This is a point source
                if (g_xray_detector.getSourceShape() != XRayDetector::PARALLEL)
                {
                    // Use a parallel source
                    g_xray_detector.setParallelBeam();

#ifndef NDEBUG
                    updateXRayImage();
                    g_xray_renderer.printEnergyFluence("parallel_beam.mha");
#endif
                }
                // This is a cubic source
                else
                {
                    g_xray_detector.setPointSource();

#ifndef NDEBUG
                    updateXRayImage();
                    g_xray_renderer.printEnergyFluence("point_source.mha");
#endif

                }

                break;

            case GLFW_KEY_2:
                // The X-ray image is not up-to-date
                g_is_xray_image_up_to_date = false;

                // This is not a line source
                if (g_xray_detector.getSourceShape() != XRayDetector::LINE)
                {
                    g_xray_detector.setLineSource(g_xray_detector.getXraySourceCentre(), VEC3(1, 0, 0), 16, 10.0*mm);

#ifndef NDEBUG
                    updateXRayImage();
                    g_xray_renderer.printEnergyFluence("line_source.mha");
#endif
}
                // This is a cubic source
                else
                {
                    g_xray_detector.setPointSource();

#ifndef NDEBUG
                    updateXRayImage();
                    g_xray_renderer.printEnergyFluence("point_source.mha");
#endif
                }

                break;

            case GLFW_KEY_3:
                // The X-ray image is not up-to-date
                g_is_xray_image_up_to_date = false;

                // This is not a square source
                if (g_xray_detector.getSourceShape() != XRayDetector::SQUARE)
                {
                    g_xray_detector.setSquareSource(g_xray_detector.getXraySourceCentre(), 10, 10.0*mm);

#ifndef NDEBUG
                    updateXRayImage();
                    g_xray_renderer.printEnergyFluence("square_source.mha");
#endif
                }
                // This is a cubic source
                else
                {
                    g_xray_detector.setPointSource();

#ifndef NDEBUG
                    updateXRayImage();
                    g_xray_renderer.printEnergyFluence("point_source.mha");
#endif
                }

                break;

            case GLFW_KEY_4:
                // The X-ray image is not up-to-date
                g_is_xray_image_up_to_date = false;

                // This is not a cubic source
                if (g_xray_detector.getSourceShape() != XRayDetector::CUBE)
                {
                    g_xray_detector.setCubicSource(g_xray_detector.getXraySourceCentre(), 5, 10.0*mm);

#ifndef NDEBUG
                    updateXRayImage();
                    g_xray_renderer.printEnergyFluence("cubic_source.mha");
#endif
                }
                // This is a cubic source
                else
                {
                    g_xray_detector.setPointSource();

#ifndef NDEBUG
                    updateXRayImage();
                    g_xray_renderer.printEnergyFluence("point_source.mha");
#endif
                }

                break;


            case GLFW_KEY_KP_ADD:
                g_zoom += 1.0 * cm;

                framebufferSizeCallback(g_p_main_window_id,
                    g_main_window_width,
                    g_main_window_height);

                std::cout << "Zoom in cm)" << g_zoom / cm << std::endl;
                break;

            case GLFW_KEY_MINUS:
            case GLFW_KEY_KP_SUBTRACT:
                g_zoom -= 1.0 * cm;

                framebufferSizeCallback(g_p_main_window_id,
                    g_main_window_width,
                    g_main_window_height);

                std::cout << "Zoom in cm)" << g_zoom / cm << std::endl;
                break;

            default:
                std::cout << "Key: " << key << std::endl;
                break;
            }
        }
}


//-----------------------------------------------------------------------
void framebufferSizeCallback(GLFWwindow* apWindow, int width, int height)
//-----------------------------------------------------------------------
{
    glViewport(0, 0, width, height);

    if (height == 0)
        {
        // Prevent divide by 0
        height = 1;
        }

    g_main_window_width = width;
    g_main_window_height = height;
}


//--------------------------------------------
void mouseButtonCallback(GLFWwindow* apWindow,
                         int aButton,
                         int aButtonState,
                         int aModifierKey)
//--------------------------------------------
{
    g_button = aButton;
    g_button_state = aButtonState;

    // Use the arc ball
    if (g_button_state == GLFW_PRESS)
        {
        g_use_arc_ball = true;
        }
    // Stop using the arc ball
    else
        {
        g_use_arc_ball = false;
        }

    double xpos(0);
    double ypos(0);
    glfwGetCursorPos (apWindow, &xpos, &ypos);

    g_last_x_position = xpos;
    g_last_y_position = ypos;

    g_current_x_position = xpos;
    g_current_y_position = ypos;
}


//--------------------------------------------------------------
void cursorPosCallback(GLFWwindow* apWindow, double x, double y)
//--------------------------------------------------------------
{
    g_current_x_position = x;
    g_current_y_position = y;

    // Update the rotation matrix if needed
    computeRotation(g_scene_rotation_matrix);
}


//-----------------------------------------------------------------------
void scrollCallback(GLFWwindow* apWindow, double xoffset, double yoffset)
//-----------------------------------------------------------------------
{
    // Scrolling along the Y-axis
    if (fabs(yoffset) > EPSILON)
        {
        g_use_arc_ball = false;
        
        g_zoom += yoffset * cm;
        
        framebufferSizeCallback(apWindow,
            g_main_window_width,
            g_main_window_height);
        }
}



/* ---------------------------------------------------------------------------- */


/* ---------------------------------------------------------------------------- */

void get_bounding_box_for_node (const aiNode* nd, 
				aiVector3D* min, 
				aiVector3D* max)
{
  aiMatrix4x4 prev;
  unsigned int n = 0, t;

  for (; n < nd->mNumMeshes; ++n) {
    const aiMesh* mesh = scene->mMeshes[nd->mMeshes[n]];
    for (t = 0; t < mesh->mNumVertices; ++t) {

      aiVector3D tmp = mesh->mVertices[t];

      min->x = aisgl_min(min->x,tmp.x);
      min->y = aisgl_min(min->y,tmp.y);
      min->z = aisgl_min(min->z,tmp.z);

      max->x = aisgl_max(max->x,tmp.x);
      max->y = aisgl_max(max->y,tmp.y);
      max->z = aisgl_max(max->z,tmp.z);
    }
  }

  for (n = 0; n < nd->mNumChildren; ++n) {
    get_bounding_box_for_node(nd->mChildren[n],min,max);
  }
}



///
void get_bounding_box (aiVector3D* min, aiVector3D* max)
{

  min->x = min->y = min->z =  1e10f;
  max->x = max->y = max->z = -1e10f;
  get_bounding_box_for_node(scene->mRootNode,min,max);
}



bool Import3DFromFile( const std::string& pFile)
{
  //check if file exists
  std::ifstream fin(pFile.c_str());
  if(!fin.fail()) {
    fin.close();
  }
  else{
    printf("Couldn't open file: %s\n", pFile.c_str());
    printf("%s\n", importer.GetErrorString());
    return false;
  }

  scene = importer.ReadFile( pFile, aiProcessPreset_TargetRealtime_Quality);

  // If the import failed, report it
  if( !scene)
    {
      printf("%s\n", importer.GetErrorString());
      return false;
    }

  printf("Import of scene %s succeeded\n",pFile.c_str());

  get_bounding_box(&scene_min, &scene_max);
  scene_center.x = (scene_min.x + scene_max.x) / 2.0f;
  scene_center.y = (scene_min.y + scene_max.y) / 2.0f;
  scene_center.z = (scene_min.z + scene_max.z) / 2.0f;
  printf("Scene center: %f, %f, %f\n", scene_center.x, scene_center.y, scene_center.z);
  //center the model
  
  //glTranslatef( -scene_center.x, -scene_center.y, -scene_center.z );
  //translate( -scene_center.x, -scene_center.y, -1500 );
		
  float tmp;
  tmp = scene_max.x-scene_min.x;
  tmp = scene_max.y - scene_min.y > tmp?scene_max.y - scene_min.y:tmp;
  tmp = scene_max.z - scene_min.z > tmp?scene_max.z - scene_min.z:tmp;
  scaleFactor = 1.4f / tmp;

  return true;
}


// Render Assimp Model
void recursive_render (const aiScene *sc, const aiNode* nd, std::vector<double>& apVertexSet, std::vector<unsigned char>& apIndexSet, aiMatrix4x4 tStack)
{
    
  //aiMatrix4x4 m = tStack * nd->mTransformation;
  aiMatrix4x4 m =  nd->mTransformation ;
  tStack = tStack * m;
  
  printf("%s\n", nd->mName.data);
  
  
  
  if(strcmp(nd->mName.data, "node-Mid_Prox")==0) {
      aiMatrix4x4 test = (aiMatrix4x4)test.RotationY(-45.0*3.14/180.0, test);
      tStack = tStack * test;
      
  }

  if(strcmp(nd->mName.data, "node-Mid_Midd")==0) {
      aiMatrix4x4 test = (aiMatrix4x4)test.RotationY(-45.0*3.14/180.0, test);
      tStack = tStack * test;
      
  }

  if(strcmp(nd->mName.data, "node-Mid_Dist")==0) {
      aiMatrix4x4 test = (aiMatrix4x4)test.RotationY(-45.0*3.14/180.0, test);
      tStack = tStack * test;
      
  }
  
    for (unsigned int n=0; n < nd->mNumMeshes; ++n){
	const aiMesh* mesh = scene->mMeshes[nd->mMeshes[n]];

            
        for (int t = 0; t < mesh->mNumFaces; ++t) {
		const aiFace* face = &mesh->mFaces[t];
		GLenum face_mode;
		for(int i = 0; i < face->mNumIndices; i++) {
                    int index = face->mIndices[i];
		
                    aiVector3D myVertex(mesh->mVertices[index].x,mesh->mVertices[index].y,mesh->mVertices[index].z);
                    aiVector3D v = tStack * myVertex;
                    
                    
                    apVertexSet.push_back( (v.x) * 600.0f);
                    apVertexSet.push_back( (v.y) * 600.0f);
                    apVertexSet.push_back( (v.z) * 600.0f);
                    
                    //printf("%f, %f, %f\n", v.x, v.y, v.z);
		}
	}
    }
  // all children
  for (unsigned int n=0; n < nd->mNumChildren; ++n){
    recursive_render(sc, nd->mChildren[n], apVertexSet, apIndexSet, tStack);
  }
}




void buildHand(		   std::vector<double>& apVertexSet,
			   std::vector<unsigned char>& apIndexSet)    {
    
    	// Clear the vertex set
	apVertexSet.clear();
	// Clear the index set
	apIndexSet.clear();
        
        //aiMatrix4x4 i;
        
        aiMatrix4x4 i;
        
        i = (aiMatrix4x4)i.RotationZ(-90.f*3.14f/180.0f, i);
        //aiMatrix4x4 j;
        //j = (aiMatrix4x4)j.RotationX(90.f*3.14f/180.0f, j);
        //i = i * j;
        
        
        
        
        recursive_render(scene, scene->mRootNode, apVertexSet, apIndexSet, i );

}



//----------------
void loadSTLFile()
//----------------
{
    g_xray_renderer.removeInnerSurfaces();
    g_xray_renderer.removeOuterSurface();

    
    buildHand(g_p_vertex_set_1, g_p_index_set_1);

        float red((float(rand()) / (RAND_MAX)) + 1);
        float green((float(rand()) / (RAND_MAX)) + 1);
        float blue((float(rand()) / (RAND_MAX)) + 1);
        float alpha(0.5);


        VEC3 base_colour(red, green, blue);
        base_colour.normalise();

        g_p_polygon_mesh_set[0].getMaterial().setAmbientColour(
            base_colour.getX() * 0.19225,
            base_colour.getY() * 0.19225,
            base_colour.getZ() * 0.19225,
            alpha);

        g_p_polygon_mesh_set[0].getMaterial().setDiffuseColour(
            base_colour.getX() * 0.50754,
            base_colour.getY() * 0.50754,
            base_colour.getZ() * 0.50754,
            alpha);

        g_p_polygon_mesh_set[0].getMaterial().setSpecularColour(
            base_colour.getX() * 0.50827,
            base_colour.getY() * 0.50827,
            base_colour.getZ() * 0.50827,
            alpha);

    g_p_polygon_mesh_set[0].getMaterial().setShininess(50);
    g_p_polygon_mesh_set[0].setHounsfieldValue(g_p_HU_set[0]);
    g_p_polygon_mesh_set[0].setExternalData(GL_TRIANGLES,
            &g_p_vertex_set_1,
            &g_p_index_set_1,
            true,
            GL_STATIC_DRAW);
    g_xray_renderer.addInnerSurface(&(g_p_polygon_mesh_set[0]));
    


    
    
    // Compute the translation vector
    g_translation_vector = g_p_polygon_mesh_set[HARD_BONE_ID].getMinCorner() +
			(g_p_polygon_mesh_set[HARD_BONE_ID].getMaxCorner() -
					g_p_polygon_mesh_set[HARD_BONE_ID].getMinCorner()) / 2.0;
	g_rotation_matrix = Matrix4x4<GLfloat>::buildRotationMatrix(
		180, VEC3(1, 0, 0));

	g_rotation_matrix *= Matrix4x4<GLfloat>::buildTranslationMatrix(
		-g_translation_vector);

    // The X-ray image is not up-to-date
    g_is_xray_image_up_to_date = false;
}


//-----------------
void loadDetector()
//-----------------
{
    g_xray_detector.setResolutionInUnitsOfLengthPerPixel(g_resolution);
    g_xray_detector.setNumberOfPixels(g_number_of_pixels);
    g_xray_detector.setDetectorPosition(g_initial_detector_position);
    g_xray_detector.setUpVector(g_initial_detector_up_vector);

    // The X-ray image is not up-to-date
    g_is_xray_image_up_to_date = false;
}


//---------------
void loadSource()
//---------------
{
    // Set the energy
    g_xray_beam.initialise(g_incident_energy);

    // Set the source position
    g_xray_detector.setXrayPointSource(g_initial_source_position);

    // Use a parallel source
    //g_xray_detector.setParallelBeam();
    g_xray_detector.setPointSource();

    // The X-ray image is not up-to-date
    g_is_xray_image_up_to_date = false;
}


//----------------------
void loadXRaySimulator()
//----------------------
{
    // Initialise the X-ray renderer
    g_xray_renderer.initialise(XRayRenderer::OPENGL,
        GL_RGB32F,
        &g_xray_detector,
        &g_xray_beam);

    // The X-ray image is not up-to-date
    g_is_xray_image_up_to_date = false;
}


//------------------------------------------
Vec3<GLfloat> getArcballVector(int x, int y)
//------------------------------------------
{
    Vec3<GLfloat> P(2.0 * float(x) / float(g_main_window_width) - 1.0,
        2.0 * float(y) / float(g_main_window_height) - 1.0,
        0);

    P.setY(-P.getY());

    float OP_squared = P.getX() * P.getX() + P.getY() * P.getY();
    if (OP_squared <= 1.0)
        {
        P.setZ(sqrt(1.0 - OP_squared));  // Pythagore
        }
    else
        {
        P.normalise();  // nearest point
        }

    return (P);
}


//--------------------------------
float radian2degree(float anAngle)
//--------------------------------
{
    return (180.0 * anAngle / gVirtualXRay::PI);
}


//--------------------------------------------
void computeRotation(MATRIX4& aRotationMatrix)
//--------------------------------------------
{
    if (g_use_arc_ball)
        {
        if (g_current_x_position != g_last_x_position ||
                g_current_y_position != g_last_y_position)
            {
            Vec3<GLfloat> va(getArcballVector(g_last_x_position,
                g_last_y_position));
            
            Vec3<GLfloat> vb(getArcballVector(g_current_x_position,
                g_current_y_position));

            float angle(g_rotation_speed *
                radian2degree(acos(std::min(1.0, va.dotProduct(vb)))));

            Vec3<GLfloat> axis_in_camera_coord(va ^ vb);

            Matrix4x4<GLfloat> camera2object(aRotationMatrix.getInverse());
            Vec3<GLfloat> axis_in_object_coord =
                camera2object * axis_in_camera_coord;

            Matrix4x4<GLfloat> rotation_matrix;
            rotation_matrix.rotate(angle, axis_in_object_coord);
            aRotationMatrix = aRotationMatrix * rotation_matrix;

            g_last_x_position = g_current_x_position;
            g_last_y_position = g_current_y_position;
            }
        }
}


//--------------------
void updateXRayImage()
//--------------------
{
    // The X-ray image is not up-to-date
    if (!g_is_xray_image_up_to_date)
        {
        float start_time(glfwGetTime());

        // Compute the X-Ray image
	    g_xray_renderer.computeImage(g_rotation_matrix);

        // Normalise the X-ray image
        g_xray_renderer.getEnergyFluenceMinMax();

        // The X-ray image is up-to-date
        g_is_xray_image_up_to_date = true;
        }
}

